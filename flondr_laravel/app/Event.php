<?php namespace App;

use Illuminate\Database\Eloquent\Model;
class Event extends Model {

	protected $table = 'ms_event';
	protected $fillable = array('name','description','date');
	public $timestamps = false;

}
