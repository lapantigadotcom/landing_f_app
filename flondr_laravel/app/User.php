<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'ms_privilige_id', 'tr_user_detail_id', 'active'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	public function detail()
    {
    	return $this->hasOne('App\UserDetail', 'id', 'tr_user_detail_id');
    }
    public function isAdmin()
	{
	    if($this->ms_privilige_id == 1)
	    	return true;
	    else
	    	return false;
	}
	public function isPublisher(){
		if($this->ms_privilige_id == 3)
	    	return true;
	    else
	    	return false;
	}
    public function isActive()
	{
	    if($this->active == 1)
	    	return true;
	    else
	    	return false;
	}
}
