<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeClub extends Model {

	protected $table = 'ms_type_club';
	protected $fillable = array('name','code');
	public $timestamps = false;

	public function club(){
		return $this->hasMany('App\Club','ms_type_club_id');
	}
}
