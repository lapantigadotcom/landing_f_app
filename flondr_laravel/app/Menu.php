<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

	protected $table = 'ms_menu';
	protected $fillable = array('code','title');
	public $timestamps = true;

	public function menuDetail()
	{
		return $this->hasMany('App\MenuDetail','ms_menu_id','id')->orderBy('order','asc');
	}
	public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
