<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use Input;

class GeraiRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rule = [
			'title' => 'required',
			'description' => 'required',
			'address' => 'required',
		];
		return $rule;
	}

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
	    return [
	        'name.required' => 'Nama gerai harus diisi',
	        'description.required'  => 'Deskripsi harus diisi',
	        'address.required'  => 'Alamat harus diisi',
	    ];
	}

}
