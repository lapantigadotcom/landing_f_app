<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\TagRequest;
use App\Tag;
use Illuminate\Http\Request;
use Session;
use Auth;
class TagController extends BasicController {
	public function __construct() {
		$this->modelName = 'App\Tag';
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$data['content'] = Tag::with('post')->get();
		return view('page.tag.index')->with('data',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data = Tag::create([
			'title' => $request->title,
			'description' => $request->description,
			'updated_at' => date('Y-m-d H:i:s'),
			'created_at' => date('Y-m-d H:i:s'),
			'ms_user_id' => Auth::user()->id
			]);
		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $request->input('title');
		// $data->logs()->save($this->log($log));
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('tag.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content']=Tag::find($id);
		$data['tag'] = Tag::all();

		return view('page.tag.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(TagRequest $request,$id)
	{

		$data = Tag::find($id);
		$data->update($request->all());

		$log = array();
		$log['action'] = 'update';
		$log['name'] = $request->input('title');
	
		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('tag.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Tag::find($id);
		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));
		$data->post()->detach();
		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('tag.index');
	}
	/* Insert From Ajax */
	public function storeAjax(Request $request)
	{
		$data = Tag::create([
			'title' => $request->title,
			'description' => $request->title,
			'updated_at' => date('Y-m-d H:i:s'),
			'created_at' => date('Y-m-d H:i:s'),
			'ms_user_id' => Auth::user()->id
			]);
		return $request->all();
		return $data->id;
	}
}
