<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 
*/

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\GeraiRequest;
use Session;
use App\gerai;
use App\geraiDetail;
use App\Media;
use App\Gender;
use App\City;

use DateTime;
use DB;

class GeraiController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = DB::table('ms_gallery')->orderBy('id', 'desc')->get();
		return view('page.gerai.index')->with('data',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$fileName = '';
		if ($request->hasFile('location'))
		{
			$file = $request->file('location');
			$extension = $file->getClientOriginalExtension();
			$fileName = 'img_'.date('Ymd_Hsi').'.'.$extension;
			$file->move('upload/media/',$fileName);
		}
		DB::table('ms_gallery')->insert([
				'location' => $fileName
			]);
		Session::flash('success','Registrasi berhasil');
		return redirect('vrs-admin/gerai');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = DB::table('ms_gerai')->where('id', $id)->first();
		if (empty($data['content']))
			redirect('vrs-admin/gerai');
		return view('page.gerai.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(GeraiRequest $request, $id)
	{
		DB::table("ms_gerai")->where('id', $id)->update([
				'title' => $request->title,
				'description' => $request->description,
				'address' => $request->address,
				'updated_at' => new DateTime()
			]);
		Session::flash('success','Data berhasil diupdate');
		return redirect('vrs-admin/gerai');
		// $this->edit($id);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		DB::table('ms_gallery')->where('id', $id)->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('gerai.index');
	}

	public function featured($id)
	{
		$data = gerai::find($id);
		if(empty($data))
		{
			return;
		}
		if($data->active == '1')
		{
			$data->active = '0';
		}else{
			$data->active = '1';
		}
		$data->save();
		return;
	}

	function getCoordinatesFromMitra($address, $city)
	{
		$coord = $this->getCoordinates($address);
		if($coord == 'null')
		{
			$coord = $this->getCoordinates($address.", ".$city);
			if($coord == 'null')
			{
				$coord = $this->getCoordinates($city);
				if($coord == 'null')
					$coord = 'null';
			}
		}
		//echo $coord."<br>";
		return $coord;
	}

	function getCoordinates($address) 
	{ 
		$key1 = "AIzaSyAS_Kkc7Af-0T-WYp4-AytXxsQCJ2PiK7A";
		$key2 = "AIzaSyAS_Kkc7Af-0T-WYp4-AytXxsQCJ2PiK7A";
		$key3 = "AIzaSyAS_Kkc7Af-0T-WYp4-AytXxsQCJ2PiK7A";
		$key = $key1;
		//echo $address."<br>";
		$address = str_replace(" ", "+", $address); // replace all the white space with "+" sign to match with google search pattern		 
		$url = "https://maps.google.com/maps/api/geocode/json?key=$key&sensor=false&address=$address";		 
		//echo $url."<br>";
		//$response = file_get_contents($url);		 
		$response = "";
		$json = json_decode($response,TRUE); //generate array object from the response from the web	
		//echo $response."<br>";
		if(count ($json['results'])	> 0)
			return ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);	 		
		return 'null';
	}

	function updateAllCoordMitra()
	{
		$allgerai = gerai::where('ms_privilige_id', '2')->get();
		foreach ($allgerai as $row) 
		{
			if($row->detail->latitude == "" || $row->detail->longitude == "" || $row->detail->latitude == "0" || $row->detail->longitude == "0")
			{
				$tmpCoord = "0,0";
				$tmpCoord = $this->getCoordinatesFromMitra($row->detail->address, $row->detail->city->name);
				if($tmpCoord == 'null')
				{
					$tmpCoord = $this->getCoordinates($row->detail->address.", ".$row->detail->city->name);
					if($tmpCoord == 'null')
					{
						$tmpCoord = $this->getCoordinates($row->detail->city->name);
						if($tmpCoord == 'null')
							$tmpCoord = "0,0";
					}
				}
	    		//echo $tmpCoord."<br>";
	    		$tmpCoord = explode(',', $tmpCoord);
	    		$latitude = $tmpCoord[0];
	    		$longitude = $tmpCoord[1];

				$row->detail->update(array(
					'latitude' => $latitude,
					'longitude' => $longitude
					));
	    		$row->detail->save();
	    		// echo $row->detail->latitude.",";
	    		// echo $row->detail->longitude."<br>";
			}
		}
	}

	function fillCustom()
	{
		$allgerai = gerai::where('ms_privilige_id', '2')->get();
		foreach ($allgerai as $row) 
		{
			if($row->detail->custom == "")
			{
				$row->detail->update(array(
					'custom' => $row->detail->panggilan
					));
	    		$row->detail->save();
			}
		}
	}

}
