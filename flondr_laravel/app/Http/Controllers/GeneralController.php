<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */
use DB;
use App\General;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\GeneralRequest;
use Illuminate\Http\Request;
use File;
use Session;

class GeneralController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = General::find(1);
		return view('page.general.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(GeneralRequest $request)
	{
		
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{

		$data = General::find($id);
		
		Session::flash('success','Data berhasil diperbarui');
		$data->update($request->all());
		return redirect()->route('general.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	public function upload(Request $request)
	{

		$data = General::find(1);
		$fileName = $data->file;
		if ($request->hasFile('file'))
		{
		    $file = $request->file('file');
			$extension = $file->getClientOriginalExtension();
			$fileName = 'img_logo.'.$extension;
			$file->move('upload/media/',$fileName);
			if(File::exists(asset('upload/media/'.$data->file)))
			{
				File::delete(asset('upload/media/'.$data->file));
			}
			$data->file = $fileName;
			$data->save();
		}
		return redirect()->route('general.index');
	}

	public function subscription(){
		$data['content'] = DB::table('ms_subscription')->first();
		return view('page.subscribe.index', compact('data'));
	}

	public function subscribe(Request $request){
		$this->validate($request, ['link' => 'required']);
		$subs = DB::table('ms_subscription')->first();
		DB::table('ms_subscription')->where('id', $subs->id)->update([
			'link' => $request->link
			]);
		return redirect('vrs-admin/brosur');
	}

}
