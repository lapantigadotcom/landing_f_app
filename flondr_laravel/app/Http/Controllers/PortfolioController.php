<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\DetailPortfolioRequest;
use App\Http\Requests\PortfolioRequest;
use App\MenuDetail;
use App\Portfolio;
use App\PortfolioCategory;
use Illuminate\Http\Request;
use Session;

class PortfolioController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$data['content'] = Portfolio::with('parent','subportfolio')->orderBy('updated_at')->get();
		return view('page.portfolio.index')->with('data',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['portfolio'] = Portfolio::where('parent_id',0)->get();
		$data['category'] = PortfolioCategory::all();
		return view('page.portfolio.create',compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data = Portfolio::create([
			'title' => $request->title,
			'parent_id' => $request->parent_id,
			'ms_categories_id' => $request->ms_categories_id,
			'description' => empty($request->description) ? '': $request->description,
			'enabled' => empty($request->enabled) ? 0 : 1,
			'featured' => empty($request->featured) ? 0 : 1
		]);

		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('portfolio.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$data['content']=Portfolio::with('detailPortfolio','detailPortfolio.media')->find($id);
		$data['category'] = PortfolioCategory::all();
		return view('page.portfolio.details',compact('data'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = Portfolio::find($id);
		$data['portfolio'] = Portfolio::where('parent_id',0)->get();
		$data['category'] = PortfolioCategory::all();
		return view('page.portfolio.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$data=Portfolio::find($id);
		$data->update([
			'title' => $request->title,
			'parent_id' => $request->parent_id,
			'ms_categories_id' => $request->ms_categories_id,
			'description' => empty($request->description)? '': $request->description
		]);

		$log = array();
		$log['action'] = 'update';
		$log['name'] = $request->input('title');
		$data->logs()->save($this->log($log));
		Session::flash('success', 'Data berhasil ditambahkan');	
		return redirect()->route('portfolio.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Portfolio::with('subportfolio','detailPortfolio')->find($id);
		foreach ($data->subportfolio as $row) {
			if($data->parent_id == 0)
				$row->parent_id = 0;
			else
				$row->parent_id = $data->parent_id;
			$row->save();
		}
		foreach ($data->detailPortfolio as $row) {
			$row->delete();
		}
		$menu = MenuDetail::where('ms_menu_type_id',3)->where('custom',$id)->get();
		foreach ($menu as $row) {
			$row->delete();
		}

		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('portfolio.index');
	}

	public function getAllData()
	{
		$data = Portfolio::all();
		return response()->json($data);
	}
	public function enabled($id =null ,$on = null)
	{
		if(empty($on) && empty($id))
			return redirect()->back();
		$data = Portfolio::find($id);
		$data->enabled = $on;
		$data->save();
		return redirect()->back();
	}
	public function featured($id)
	{
		$data = Portfolio::find($id);
		if(empty($data))
		{
			return;
		}
		if($data->featured == '1')
		{
			$data->featured = '0';
		}else{
			$data->featured = '1';
		}
		$data->save();
		return;
	}
}