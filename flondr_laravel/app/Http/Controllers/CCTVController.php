<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\MediaRequest;
use App\Media;
use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;
use Session;
use Image;
use Input;
use DB;
use DateTime;

class CCTVController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $path;
	function __construct() {
		$this->path = 'upload/media/';
	}
	public function index()
	{
		$data['content'] = DB::table('ms_cctv')->orderBy('id', 'desc')->get();
		return view('page.cctv.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		DB::table('ms_cctv')->insert([
				'title' => Input::get('title'),
				'description' => Input::get('description'),
				'embed' => Input::get('embed'),
				'ms_media_id' => Input::get('ms_media_id'),
				'created_at' => new DateTime(),
				'updated_at' => new DateTime()
			]);
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.cctv.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content']=DB::table('ms_cctv')->find($id);
		return view('page.cctv.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		DB::table('ms_cctv')->where('id', $id)->update([
				'title' => Input::get('title'),
				'description' => Input::get('description'),
				'embed' => Input::get('embed'),
				'ms_media_id' => Input::get('ms_media_id'),
				'updated_at' => new DateTime()
			]);
		return redirect()->route('vrs-admin.cctv.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		DB::table('ms_cctv')->where('id', $id)->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.cctv.index');
	}
	
}
