<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Club;
use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ClubRequest;
use Illuminate\Http\Request;
use Session;
use Image;
use File;

class ClubController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $path;
	function __construct() {
		$this->path = 'upload/member/';
	}
	public function index()
	{
		$data['content'] = Club::orderBy('created_at')->get();
		return view('page.club.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$data['typeclub'] = \App\TypeClub::lists('name','id');
		return view('page.club.create', compact('data'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ClubRequest $request)
	{
		$data = new Club;
		$data->name = $request->input('name');
		$data->city = $request->input('city');
		$data->reward = $request->input('reward');
		$data->ms_type_club_id = $request->input('ms_type_club_id');
		if ($request->hasFile('photo'))
		{
		    $file = $request->file('photo');
			$extension = $file->getClientOriginalExtension();
			$fileName = 'user_'.date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path,$fileName);
			// $data->mime = $file->getClientMimeType();
			$img = Image::make($this->path.$fileName);
			$img->resize(200, 200);
			$img->save($this->path.$fileName, 60);
			$data->photo = $fileName;
		}
		$data->save();
		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $request->input('name');
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.club.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['typeclub'] = \App\TypeClub::lists('name','id');
		$data['content'] = Club::find($id);
		return view('page.club.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ClubRequest $request, $id)
	{
		$data = Club::find($id);
		
		$data->name = $request->input('name');
		$data->city = $request->input('city');
		$data->reward = $request->input('reward');
		$data->ms_type_club_id = $request->input('ms_type_club_id');
		if ($request->hasFile('photo'))
		{
		    $file = $request->file('photo');
			$extension = $file->getClientOriginalExtension();
			$fileName = 'user_'.date('Ymd_Hsi').'.'.$extension;

			$log = array();
			$log['action'] = 'update';
			$log['name'] = $data->photo.' - '.$fileName;
			$data->logs()->save($this->log($log));


			$file->move($this->path,$fileName);
			$img = Image::make($this->path.$fileName);
			$img->resize(200, 200);
			$img->save($this->path.$fileName, 60);
			if(File::exists('upload/member/'.$data->photo))
			{
				File::delete('upload/member/'.$data->photo);
			}

			// $data->mime = $file->getClientMimeType();
			$data->photo = $fileName;
		}else{
			$log = array();
			$log['action'] = 'update';
			$log['name'] = $data->name;
			$data->logs()->save($this->log($log));
		}
		$data->save();

		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.club.index');
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Club::find($id);
		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->name;
		$data->logs()->save($this->log($log));
		if(File::exists('upload/member/'.$data->photo))
			{
				File::delete('upload/member/'.$data->photo);
			}

		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.club.index');
	}
	public function enabled($id=null)
	{
		if(empty($id))
			return;
		$data = Club::where('featured','1')->first();
		if(!empty($data))
		{
			$data->featured = 0;
			$data->save();
		}
		$data = Club::find($id);
		$data->featured = '1';
		$data->save();
		return;
	}

}
