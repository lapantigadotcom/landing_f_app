<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

use App\DailyCP;
class DailyCPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['content'] = DailyCP::first();
        return view('page.dailycp.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $id = 1;
        $data['content'] = DailyCP::find($id);
        return view('page.dailycp.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = DailyCP::find($id);
        if (empty($data))
            return redirect()->route('dailycp.edit', [1]);
        $data->text = $request->text;
        try {
            $data->save();
        } catch (Exception $e) {
            return redirect()->route('dailycp.edit', [1]);
        }
        Session::push('success','Data berhasil diperbarui');
        return redirect()->route('dailycp.edit', [1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
