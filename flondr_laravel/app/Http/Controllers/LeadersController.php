<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 
*/

use App\Http\Requests;
use App\Http\Requests\LeadersRequest;
use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Achievement;
use App\LeadersAchievement;
use App\Leaders;

use Illuminate\Http\Request;
use Storage;
use Session;
use Input;

class LeadersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Leaders::all();
		return view('page.leader.index')->with('data',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */

	public function create()
	{
		$data['achievement'] = Achievement::all();
		return view('page.leader.create')->with('data',$data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data=Leaders::create($request->all());

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('testi.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		echo "string";
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['achievement'] = Achievement::all();
		$data['content'] = Leaders::all()->find($id);
		return view('page.leader.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
public function update(Request $request,$id)
	{

		$data=Leaders::find($id);
		$data->update($request->all());
		$data->achievements()->detach();
		if(is_array($request->achievements) and count($request->achievements) > 0)
		foreach ($request->achievements as $row) {
			$data->achievements()->attach($row);
		}

		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('testi.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data=Leaders::where('id', $id)->delete();

		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('testi.index');
	}

	public function getAllAjax()
	{
		$data = Leaders::all();
		return response()->json($data);
	}
}
