<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
use DB;
use Input;
use App\Theme;
use Mail;
class AduanController extends BasicController {
	protected $theme;
	protected $data;
	protected $email;
	protected $nama;
	protected $phone;
	protected $subject;

	public function __construct($foo = null) {
		$theme_t = Theme::where('active','1')->first();
		$this->theme = $theme_t->code;
	}
	public function index(){
		$data['content'] = DB::table('ms_aduan')->get();
		return view('page.aduan.index',compact('data'));
	}

	public function replyForm($id){
		$data['content'] = DB::table('ms_aduan')->where('id', $id)->first();
		return view('page.aduan.reply',compact('data'));
	}

	public function reply($id){
		$aduan = DB::table('ms_aduan')
					->where('ms_aduan.id', $id)
					->first();
		$toSent = ['content' => Input::get('balasan'), 'aduan' => $aduan];
		if (!empty($aduan)){
			$this->email = $aduan->email;
			$this->nama = $aduan->nama;
			$this->phone = $aduan->phone;
			$this->subject = Input::get('subject'. " (Balasan respon cepat)");
			Mail::send('mail', $toSent, function ($message) {
			    $message->to($this->email, $this->nama)->subject($this->subject);
			});
			Session::flash('success','Berhasil mengirim email');
		} else {
			Session::flash('error','Data tidak ditemukan');
		}
		return redirect('vrs-admin/aduan');
	}
	public function subscription($id){
		$aduan = DB::table('ms_aduan')
					->where('ms_aduan.id', $id)
					->first();
		$toSent = ['content' => Input::get('balasan'), 'aduan' => $aduan];
		if (!empty($aduan)){
			$this->email = $aduan->email;
			$this->nama = $aduan->nama;
			$this->phone = $aduan->phone;
			$this->subject = Input::get('subject'. " (Balasan respon cepat)");
			Mail::send('mail', $toSent, function ($message) {
			    $message->to($this->email, $this->nama)->subject($this->subject);
			});
			Session::flash('success','Berhasil mengirim email');
		} else {
			Session::flash('error','Data tidak ditemukan');
		}
		return redirect('vrs-admin/aduan');
	}

	public function reply2($id){
		$aduan = DB::table('ms_aduan')
					->where('ms_aduan.id', $id)
					->join('ms_kategori_aduan', 'ms_aduan.ms_kategori_aduan_id', '=', 'ms_kategori_aduan.id')
					->select('ms_aduan.id', 'ms_aduan.nama_lengkap', 'ms_aduan.email', 'ms_aduan.hp', 'ms_aduan.alamat', 'ms_aduan.isi', 'ms_kategori_aduan.title')
					->first();
		$toSent = ['content' => 'lkjasdlfkjaskfldj', 'aduan' => $aduan];
		return view('theme.'.$this->theme.'.mail', $toSent);
	}

	public function destroys($id){
		DB::table('ms_aduan')->where('id', $id)->delete();
		return redirect('vrs-admin/aduan');
	}
}