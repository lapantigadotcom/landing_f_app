<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 
*/

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CategoryRequest;
use App\Achievement;
use App\LeadersAchievement;
use App\Leaders;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Session;

class AchievementController extends BasicController {

	public function __construct() {
		$this->modelName = 'App\Achievement';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$data['content'] = Achievement::with('leader')->get();
		return view('page.achievement.index')->with('data',$data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data = Achievement::create($request->all());
		// $log = array();
		// $log['action'] = 'insert';
		// $log['name'] = $request->input('title');
		// $data->logs()->save($this->log($log));
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.achievement.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content']=Achievement::find($id);
		$data['category'] = Achievement::all();

		return view('page.achievement.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{

		$data = Achievement::find($id);
		$data->update($request->all());

		// $log = array();
		// $log['action'] = 'update';
		// $log['name'] = $request->input('title');
		// $data->logs()->save($this->log($log));


		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('vrs-admin.achievement.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Achievement::find($id);
		// $log = array();
		// $log['action'] = 'delete';
		// $log['name'] = $data->title;
		// $data->logs()->save($this->log($log));

		//$data->leader()->detach();
		$data->delete();
		$leadersAchievement = LeadersAchievement::where('ms_achievement_id',$id)->get();
		foreach ($leadersAchievement as $row) {
			$leadersAchievement->delete();
		}

		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.achievement.index');
	}
	/* Insert From Ajax */
	public function storeAjax(Request $request)
	{
		$data = Achievement::create($request->all());


		return $data->id;
	}
	public function getAllAjax()
	{
		$data = Achievement::all();

		return  response()->json($data);
	}
}
