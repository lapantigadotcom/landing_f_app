<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Post;
use App\Portfolio;
use App\DailyCP;
use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\AduanRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\MitraRequest;
use App\Http\Requests\PostRequest;
use App\Http\Requests\ContactUsRequest;
use App\Http\Requests\TestimoniRequest;
use App\PortfolioCategory;
use App\Leaders;
use App\Rss;
use App\Slider;
use App\Theme;
use App\General;
use App\Products;
use App\Contact;
use App\Gender;
use App\City;
use App\User;
use App\UserDetail;
use App\Media;
use App\Event;
use App\FilePdf;
//use FeedReader;
use Session;
use Image;
use Auth;
use Redirect;
use Mail;
use Input;
use DateTime;
//use LaravelAnalytics;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($foo = null) {
        $this->path = 'upload/media/';
        $theme_t = Theme::where('active','1')->first();
        $this->api_shipping = 'a6d63f4a881977714ca6ffef85e955e9';
        $this->theme = $theme_t->code;
        $this->data['contact'] = Contact::where('featured','1')->first();
        $this->data['products'] = Products::with('media')->where('ms_categories_id', 1)->get()->take(8);
        $this->data['leaders'] = Leaders::with('achievements')->orderBy('rank','asc')->get();
        $this->data['about_sekilas'] = Post::with('category')->whereHas('category', function($q)
        {
            $q->where('ms_category_id', '9'); // category 9 = sekilas
        })->get()->take(1);
        $this->data['latest_event'] = Post::with('category')->whereHas('category', function($q)
        {     
            $q->where('ms_category_id', '27'); // category 9 = sekilas
        })->get()->take(5);


        $this->data['latest'] = Post::with('category')->where('featured','1')->whereHas('category', function($q)
        {
            $q->where('ms_category_id', '8'); // category 8 = kelebihan
        })->orderBy('created_at','desc')->get()->take(4);
        $this->data['latest_events'] = Post::with('category')->whereHas('category', function($q)
        {
            $q->where('ms_category_id', '66'); // category 2 = event
        })->orderBy('created_at','desc')->get()->take(4);
        $this->data['slider'] = Slider::with('media')->join('ms_medias', 'ms_sliders.ms_media_id', '=', 'ms_medias.id')->orderBy('ms_sliders.created_at','desc')->get();
        $this->data['slider_posts'] = Post::with('category')->whereHas('category', function($q)
        {
            $q->where('ms_category_id', '1'); // category 1 = berita
        })->orderBy('created_at','desc')->get()->take(3);
        $this->data['portfolio2'] = Portfolio::where('featured', 1)
        ->join('tr_detail_portfolios', 'ms_portfolios.id', '=', 'tr_detail_portfolios.ms_portfolio_id')
        ->join('ms_medias', 'tr_detail_portfolios.ms_media_id', '=', 'ms_medias.id')
        ->where('tr_detail_portfolios.type', 1)
        ->orderBy('tr_detail_portfolios.id', 'desc')
        ->limit(8)
        ->get();
        $this->data['portfolio'] = Portfolio::with('detailPortfolio','detailPortfolio.media')->where('featured','1')->whereHas('detailPortfolio', function($q)
        {
            $q->where('type', '1');
        })->get();
        
        $this->data['event'] = Event::orderBy('date','asc')->get();

    }
    



    public function category($id=null,$page = 'category',Request $request)
    {   $data = $this->data;
        $data['english'] = false;
        if ($request->ln == 'eng')
            $data['english'] = true;
         $category = Category::with('subcategory','subcategory.subcategory','subcategory.subcategory')->find($id);
        $data['context'] = 'Kategori '.ucfirst($category->title);
        $data['tags'] = \App\Tag::all();
        $data['featured_portfolio'] = \App\Portfolio::where('featured','1')->whereHas('detailPortfolio', function($q)
        {
            $q->where('type', '1');

        })->first();
        // dd($data['latest_posts']);
        $arr_Category = array();
        array_push($arr_Category, $id);
        if($category->subcategory()->count() > 0)
        {
            foreach ($category->subcategory as $row) {
                array_push($arr_Category, $row->id);
                if($row->subcategory()->count() > 0){
                    foreach ($row->subcategory as $val) {
                        array_push($arr_Category, $val->id);
                    }
                }
            }
        }
        
        // $data['content'] = Category::with('post','post.mediaPost')->whereIn('id',$arr_Category)->paginate(6);
        $data['content'] = Post::with('mediaPost','user')->whereHas('category',function($q) use ($arr_Category){
            $q->whereIn('ms_categories.id',$arr_Category);
        })->orderBy('created_at','desc')
        ->paginate(3);
        $data['content']->setPath("kategori=".$id);
        return view('theme.'.$this->theme.".".$page,compact('data'));

    }
    public function post($id=null,$page = 'detail_posting',Request $request) 
    {
        $data = $this->data;
        $data['english'] = false;
        if ($request->ln == 'eng')
            $data['english'] = true;
        $data['content'] = Post::with('mediaPost','portfolio','portfolio.detailPortfolio','portfolio.detailPortfolio.media')->find($id);
        if (!empty($data['content'])){
            if($data['content']->ms_portfolio_id!='0' || $data['content']->ms_portfolio_id!='' )

                $data['portfolio'] = Portfolio::with('detailPortfolio','detailPortfolio.media')->where('id','<>',$id)->whereHas('detailPortfolio', function($q)
                {
                    $q->where('type', '1');

                })->get();
            else
                $data['portfolio'] = Portfolio::with('detailPortfolio','detailPortfolio.media')->whereHas('detailPortfolio', function($q)
                {
                    $q->where('type', '1');

                })->get();

            return view('theme.'.$this->theme.".".$page,compact('data'));
        } 
        return redirect()->route('notfound');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function map(Request $request)
    {
        return view('flondr.partials.map_gsm');
    }
    // public function leaders()
    // {
    //     $data = $this->data;
    //     $page = 'leaders';
    //     $data['content'] = Leaders::orderBy('rank', 'asc');

    //     $data['leaders'] = $data['content']->paginate(12);
    //     $data['leaders']->setPath("");

    //     return view('theme.'.$this->theme.".".$page,compact('data'));
    // }
    public function adukan(Request $request){
        $this->validate($request, [
            'nama_lengkap' => 'required',
            'email' => 'required',
            'isi' => 'required']);
        DB::table('ms_aduan')->insert([
                'nama' => Input::get('nama_lengkap'),
                'email' => Input::get('email'),
                'phone' => Input::get('phone'),
                'isi' => Input::get('isi'),
                'status' => 0,
                'created_at' => new DateTime(),
            ]);
        Session::flash('success','Successfully Sent. We will Contact Shortly.');
        return redirect('/');
    }
    public function leaders()
    {
        $data = $this->data;
        $page = 'leaders';
        $data['content'] = Leaders::orderBy('id', 'asc');

        $data['leaders'] = $data['content']->paginate(12);
        $data['leaders']->setPath("");

        return view('theme.'.$this->theme.".".$page,compact('data'));
    }

    public function index(Request $request)
    {
        $data['galleries'] = DB::table('ms_gallery')->orderBy('id', 'desc')->limit(5)->get();
        // First Tower
        $data['section_1'] = Post::find(2);
        $data['intro_image'] = Portfolio::find(28);
        
        // Floorplan and Unit Gallery
        $data['section_2'] = Portfolio::find(26);

        // Headlines
        $data['section_3'] = Category::find(27);

        //testimoni
        $data['testi'] = Leaders::join('ms_medias', 'ms_leaders.ms_media_id', '=', 'ms_medias.id')
        ->select('ms_leaders.name', 'ms_leaders.isi', 'ms_medias.file')
        ->orderBy('ms_leaders.id')->limit(3)
        ->get();


        // Events
        $data['section_4'] = Category::find(28);
        
        // Gallery Opening
        $data['section_5'] = Portfolio::find(21);

        // Gallery
        $data['section_6'] = Portfolio::find(22);

        // Location (section 7) and Public Places (section 8) Hardcoded

        // About Us
        $data['section_9'] = Post::find(10);

        // Experience Opening
        $data['section_10']['gallery'] = Portfolio::find(25);
        $data['section_10']['posts'] = Category::find(34);

        // About Us 2 
        $data['section_11'] = Post::find(29);
        $data['section_12'] = Post::find(30);
        
        // Image Middle
        $data['image_middle'] = Portfolio::find(27);
                $data['testimoni'] = Leaders::with('achievements')->orderBy('id','asc')->get();

        // Contact
        $data['footer'] = DailyCP::find(1);
        $data['english'] = false;
        if ($request->ln == 'eng')
            $data['english'] = true;

        return view('flondr.index', compact('data'));
    }

    function subscribe(Request $request){
        $this->validate($request, ['email' => 'required|email']);
        $links = '';
        $subscription_data = DB::table('ms_subscription')->first();
        if (!empty($subscription_data))
            $links = $subscription_data->link;

        $toSent = ['content' => $links];
        $this->from = 'Flondr App';
        $this->email = $request->email;
        $this->subject = 'Subscription Flondr App';
        Mail::send('subscribe', $toSent, function ($message) {
            $message->to($this->email, $this->email)->subject($this->subject);
        });
        Session::flash('success','Check your email for our subscription');
        return redirect('/');
    }
}
