<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\BasicController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\MediaRequest;
use App\Media;
use File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Storage;
use Session;
use Input;

class MediaController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	protected $path;
	function __construct() {
		$this->path = 'upload/media/';
	}
	public function index()
	{
		$data['content'] = Media::orderBy('created_at','desc')->get();
		return view('page.media.index',compact('data'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('page.media.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, ['file' => 'required|image',
			'caption' => 'required',
			'description' => 'required',
			'alternate_text' => 'required']);
		
		$data = New Media;
		$fileName = '';
		if ($request->hasFile('file'))
		{
		    $file = $request->file('file');
			$extension = $file->getClientOriginalExtension();
			$fileName = 'img_'.date('Ymd_Hsi').'.'.$extension;
			$file->move($this->path,$fileName);
			$data->mime = $file->getClientMimeType();
		}
		
		$data->file = $fileName;
		$data->caption = $request->input('caption');
		$data->description = $request->input('description');
		$data->alternate_text = empty($request->alternate_text) ? 'grandshamaya-image' : $request->alternate_text;
		$data->isumum = empty($request->isumum) ? 0 : $request->isumum;
		$data->save();

		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $fileName;
		// $data->logs()->save($this->log($log));
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('media.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content']=Media::find($id);
		return view('page.media.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$data = Media::find($id);
		$fileName = $data->file;

		if ($request->hasFile('file'))
		{
		    $file = $request->file('file');
			$extension = $file->getClientOriginalExtension();
			$fileName = 'img_'.date('Ymd_Hsi').'.'.$extension;

			$log = array();
			$log['action'] = 'update';
			$log['name'] = $data->file.' - '.$fileName;
			$data->logs()->save($this->log($log));


			$file->move($this->path,$fileName);
			if(File::exists('upload/media/'.$data->file))
			{
				File::delete('upload/media/'.$data->file);
			}

			$data->mime = $file->getClientMimeType();
		}else{
			$log = array();
			$log['action'] = 'update';
			$log['name'] = $data->caption;
			$data->logs()->save($this->log($log));
		}
		
		$data->file = $fileName;
		$data->caption = $request->input('caption');
		$data->description = $request->input('description');
		$data->save();

		

		return redirect()->route('media.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Media::find($id);
		\App\Post::where('ms_media_id',$id)->update(array('ms_media_id' => '0'));
		\App\DetailPortfolio::where('ms_media_id',$id)->delete();
		\App\Slider::where('ms_media_id',$id)->delete();
		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->file;
		$data->logs()->save($this->log($log));
		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('media.index');
	}

	public function getAllData()
	{
		$offset = Input::get('offset');
		$data = null;
		if(empty($offset))
			$data = Media::orderBy('created_at','desc')->get()->take(12);
		else
			$data = Media::orderBy('created_at','desc')->skip($offset)->take(12)->get();
		return $data;
	}
	public function storeAjax(MediaRequest $request){
		return response($request->all());
		$file = $request->file('file');
		$extension = $file->getClientOriginalExtension();
		$fileName = 'img_'.date('Ymd_Hsi').'.'.$extension;
		$file->move($this->path,$fileName);
		/*$img = Image::make($this->path.$fileName);
		$img->save($this->path.$fileName, 60);*/
		$data = new Media;
		$data->file = $fileName;
		$data->mime = $file->getClientMimeType();
		$data->save();
		return response($data);
		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $data->file;
		$data->logs()->save($this->log($log));

		return $data->file;
	}
	public function getMedia($fileName)
	{
		$data = Media::where('file',$fileName)->firstOrFail();
		$file = Storage::disk('local')->get($data->file);

		return (new Response($file,200))
			->header('Content-Type', $file->mime);
	}
}
