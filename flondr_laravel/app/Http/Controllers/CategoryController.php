<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CategoryRequest;
use App\MenuDetail;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Session;
use Auth;

class CategoryController extends BasicController {

	public function __construct() {
		$this->modelName = 'App\Category';
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$data['content'] = Category::with('post')->get();
		return view('page.category.index')->with('data',$data);
		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$data = Category::create([
			'title' => $request->title,
			'parent_id' => $request->parent_id,
			'description' => empty($request->description)? '' : $request->description,
			'ms_user_id' => Auth::user()->id
		]);
		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $request->input('title');
		$data->logs()->save($this->log($log));
		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('category.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content']=Category::find($id);
		$data['category'] = Category::all();

		return view('page.category.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{

		$data = Category::find($id);
		$data->update($request->all());

		$log = array();
		$log['action'] = 'update';
		$log['name'] = $request->input('title');
		$data->logs()->save($this->log($log));


		Session::flash('success','Data berhasil diperbarui');
		return redirect()->route('category.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Category::find($id);
		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		$data->post()->detach();
		$data->delete();
		$menu = MenuDetail::where('ms_menu_type_id',1)->where('custom',$id)->get();
		foreach ($menu as $row) {
			$row->delete();
		}

		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('category.index');
	}
	/* Insert From Ajax */
	public function storeAjax(Request $request)
	{
		$data = Category::create($request->all());
		return $data->id;
	}
	public function getAllAjax()
	{
		$data = Category::all();
		return  response()->json($data);
	}
}
