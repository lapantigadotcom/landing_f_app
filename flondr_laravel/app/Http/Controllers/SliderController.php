<?php namespace App\Http\Controllers;
/*
lapantiga.com | Web & Mobile App Developer. Jl. Gubeng Kertajaya 9C no.27 A Surabaya - Indonesia, +62.856.3437.495 */

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\SliderRequest;
use App\Slider;
use Illuminate\Http\Request;
use Session;

use Input;
class SliderController extends BasicController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['content'] = Slider::with('media')->orderBy('created_at','desc')->get();
		return view('page.slider.index',compact('data'));
	}
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(SliderRequest $request)
	{
		//dd(Input::all());
		$data = Slider::create($request->all());

		$log = array();
		$log['action'] = 'insert';
		$log['name'] = $data->title;
		//$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.slider.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data['content'] = Slider::with('media')->find($id);
		return view('page.slider.edit',compact('data'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(SliderRequest $request, $id)
	{
		$data = Slider::find($id);
		$data->update($request->all());

		$log = array();
		$log['action'] = 'update';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		Session::flash('success','Data berhasil ditambahkan');
		return redirect()->route('vrs-admin.slider.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$data = Slider::find($id);

		$log = array();
		$log['action'] = 'delete';
		$log['name'] = $data->title;
		$data->logs()->save($this->log($log));

		$data->delete();
		Session::flash('success','Data berhasil dihapus');
		return redirect()->route('vrs-admin.slider.index');
	}
}