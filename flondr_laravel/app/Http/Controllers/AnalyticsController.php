<?php
namespace App\Http\Controllers;
use Spatie\Analytics;
class AnalyticsController extends Controller {
	public function index(){
		echo Analytics::fetchMostVisitedPages(Period::days(7));
	}
}
?>