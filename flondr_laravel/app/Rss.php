<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Rss extends Model {

	protected $table = 'ms_rss';
	protected $fillable = array('ms_user_id','title','link','featured');
	public $timestamps = true;

	public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
