<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model {

	protected $table = 'ms_socials';
	protected $fillable = array('name','link','code');
	public $timestamps = true;
	public function logs()
    {
        return $this->morphMany('App\Log', 'logable');
    }
}
