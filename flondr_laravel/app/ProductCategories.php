<?php namespace App;

use Illuminate\Database\Eloquent\Model;
class ProductCategories extends Model {

	protected $table = 'ms_product_categories';
	protected $fillable = array('name','description');
	public $timestamps = false;
	public function product()
    {
        return $this->hasMany('App\Products','ms_categories_id');
        //return $this->belongsToMany('App\Post','tr_category_posts','ms_category_id','ms_post_id');
    }
}
