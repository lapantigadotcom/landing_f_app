<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

	protected $table = 'tr_comments';
	protected $fillable = array('ms_post_id','name','title','email','description','enabled');
	public $timestamps = true;

	public function post()
	{
		return $this->belongsTo('App\Post','ms_post_id','id');
	}

}
