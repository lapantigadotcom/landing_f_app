<?php namespace App;

use Illuminate\Database\Eloquent\Model;
class Products extends Model {

	protected $table = 'ms_products';
	protected $fillable = array('name','description','ms_media_id','ms_categories_id');
	public $timestamps = false;

    public function media()
    {
        return $this->hasOne('\App\Media', 'id', 'ms_media_id');
    }
    public function category()
    {
        return $this->hasOne('\App\ProductCategories', 'id', 'ms_categories_id');
    }
}
