<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Province;
use App\General;
use App\User;

class City extends Model {

	protected $table = 'ms_cities';
//	protected $guarded = ['id'];
    public $timestamps = false;

    public function province()
    {
    	return $this->belongsTo('App\Province', 'ms_province_id', 'id');
    }

    public function generals()
    {
    	return $this->hasMany('App\General', 'ms_city_id', 'id');
    }

    public function users()
    {
        return $this->hasMany('App\User', 'ms_city_id', 'id');
    }

}
