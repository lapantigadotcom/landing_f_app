<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Club extends Model {

	protected $table = 'ms_clubs';
	protected $fillable = array('name','city','reward','photo','ms_type_club_id');
	public $timestamps = true;
	public function logs()
	    {
	        return $this->morphMany('App\Log', 'logable');
	    }
	public function typeClub()
    {
    	return $this->belongsTo('App\TypeClub', 'ms_type_club_id', 'id');
    }
}
