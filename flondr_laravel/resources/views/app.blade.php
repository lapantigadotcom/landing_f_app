<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Flondr Panel</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        @include('partials.head')
    </head>
    <body class="skin-blue">
        <div class="wrapper">
            @include('partials.nav')
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                @include('partials.side-bar')
            </aside>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                @yield('content')
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->    
        <!-- add new calendar event modal -->
        @include('partials.footer')
    </body>
</html>
