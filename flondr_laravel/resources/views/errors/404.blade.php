 
 <!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta name="author" content="lapantiga.com">

<link href="{{ asset('/images/favicon32.png')}}" rel="shortcut icon" type="image/png">
<link href="{{ asset('/images/apple-touch-icon.png')}}" rel="apple-touch-icon">
<link href="{{ asset('/images/apple-touch-icon-72x72.png')}}" rel="apple-touch-icon" sizes="72x72">
<link href="{{ asset('/images/apple-touch-icon-114x114.png')}}" rel="apple-touch-icon" sizes="114x114">
<link href="{{ asset('/images/apple-touch-icon-144x144.png')}}" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="{{ asset('/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{ asset('/css/animate.css')}}" rel="stylesheet" type="text/css">
<!-- <link href="{{ asset('/css/additional.css')}}" rel="stylesheet" type="text/css"> -->

<link href="{{ asset('/css/css-plugin-collections.css')}}" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link id="menuzord-menu-skins" href="{{ asset('/css/menuzord-skins/menuzord-rounded-boxed.css')}}" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="{{ asset('/css/style-main.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="{{ asset('/css/preloader.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="{{ asset('/css/custom-bootstrap-margin-padding.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="{{ asset('/css/responsive.css')}}" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<link href="{{ asset('/css/style.css')}}" rel="stylesheet" type="text/css">

<!-- Revolution Slider 5.x CSS settings -->
<link  href="{{ asset('/js/revolution-slider/css/settings.css')}}" rel="stylesheet" type="text/css"/>
<link  href="{{ asset('/js/revolution-slider/css/layers.css')}}" rel="stylesheet" type="text/css"/>
<link  href="{{ asset('/js/revolution-slider/css/navigation.css')}}" rel="stylesheet" type="text/css"/>
<title>Maaf.......Error 404. Halaman / file yang dicari tidak ditemukan</title>
<!-- CSS | Theme Color -->
<link href="{{ asset('/css/colors/theme-skin-color-set-5.css')}}" rel="stylesheet" type="text/css">
</head>
<body class="">
<div id="wrapper" class="clearfix">
   <div class="main-content"> 
     <section id="home" class="fullscreen ">
      <div class="display-table text-center">
        <div class="display-table-cell">
          <div class="container pt-0 pb-0">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                <h1 class="font-150 text-theme-colored mt-0 mb-0"><i class="fa fa-map-signs text-theme-color-2"></i>404!</h1>
                <h2 class="mt-0">Whopppsss! Page Not Found</h2>
                <p>Halaman / file yang dicari tidak ditemukan</p>
                <a class="btn btn-border btn-gray btn-transparent btn-circled" href="{!! URL::to('/') !!}">Back to Home</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
 

  <script src="{{ asset('/js/jquery-2.2.0.min.js')}}"></script>
<script src="{{ asset('/js/jquery-ui.min.js')}}"></script>
<script src="{{ asset('/js/bootstrap.min.js')}}"></script>
 <script src="{{ asset('js/jquery-plugin-collection.js')}}"></script>
 <script src="{{ asset('js/custom.js')}}"></script>
<script src="{{ asset('js/extra.js')}}"></script>

 <script src="{{ asset('js/revolution-slider/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{ asset('js/revolution-slider/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{ asset('js/extra-rev-slider.js')}}"></script> 
</body>
</html>
