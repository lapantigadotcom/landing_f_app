<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title>Flondr | Connecting Places </title>
 <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no"/>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
 <meta name="og:site_name" content="Flondr | Connecting Places">
 <meta name="copyright" content="PT. Gatam Perkasa Media">
 <meta name="description" content="Flondr is the answer of everything you need in your daily life. We provide information about many type of business by giving you the address, phone number, working hours, direction, social media (if applicable). You don’t have to wonder when the stores open or closed or false direction. Flondr is the local guide to finding the perfect place to eat, shop, doing business, and play. Need Help? Contact Our Email : support@flondr.com" />
 <meta name="keywords" content="Flondr, local guide app, flondr surabaya, flondr android, places, business place, outlet, resto, food, food review" />
 <meta name="author" content="lapantiga.com" />
 @include('theme.flondr_landing.partials.head')
</head>
<body style="background:#2d141e">
  @include('theme.flondr_landing.partials.menu')
  @include('theme.flondr_landing.layout.slider')
  @include('theme.flondr_landing.layout.flondr_features')
  <div class="clearfix"> </div>
  @include('theme.flondr_landing.layout.get_touch')
  @include('theme.flondr_landing.partials.footer')
  @include('theme.flondr_landing.partials.script')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script src="plugins/slick-1.6.0/slick/slick.min.js"></script>
  <script type="text/javascript">
    @if(!empty(Session::get('success')))
    $('#modal-display').click();
    @endif
  </script>
</body>
</html>