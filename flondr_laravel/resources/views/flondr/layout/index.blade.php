<!DOCTYPE html>
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <title>Flondr | Connecting Places </title>
 <meta name="viewport" content="width=device-width, initial-scale=1.0 user-scalable=no"/>
 <meta name="author" content="lapantiga.com">
 <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
 <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
 <meta name="description" content=" " />
 <meta name="keywords" content=" " />
 <meta name="author" content="lapantiga.com" />
 @include('theme.flondr_landing.partials.head')
</head>
<body style="background:#2d141e">
  @include('theme.flondr_landing.partials.menu')
  @include('theme.flondr_landing.layout.slider')
  @include('theme.flondr_landing.layout.flondr_features')
  <div class="clearfix"> </div>
  @include('theme.flondr_landing.layout.get_touch')
  @include('theme.flondr_landing.partials.footer')
  @include('theme.flondr_landing.layout.form_contact_footer')
  @include('theme.flondr_landing.partials.script')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <script src="plugins/slick-1.6.0/slick/slick.min.js"></script>
  <script type="text/javascript">
    @if(!empty(Session::get('success')))
    $('#modal-display').click();
    @endif
  </script>
</body>
</html>