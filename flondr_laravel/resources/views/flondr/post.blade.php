@if(!empty($data['content']))
<div class="col-md-12">
  <div class="col-md-12">
    <br>
    <center>
      @if(isset($data['content']->mediaPost->file))
      <img src="{{ asset('upload/media/'.$data['content']->mediaPost->file) }}" style="width: 100%;">
      @endif
    </center>
    <article>
      <h4> 
        @if ($data['english'])
        {!! $data['content']->title_eng !!}
        @else
        {!! $data['content']->title !!}
        @endif
      </h4>  <hr>
      <span style="color:#B68547">
        <i class="glyphicon glyphicon-calendar"></i> {{ date("d M Y" ,strtotime($data['content']->created_at)) }}
        <i class="glyphicon glyphicon-user"></i> {{ $data['content']->user->name }}</span>
        <p>
          @if ($data['english'])
          {!! $data['content']->eng_description !!}
          @else
          {!! $data['content']->description !!}
          @endif<hr>
        </p>
      </article>
    </div></div>
    <p class="footer-watermark" style="text-align:center">COPYRIGHT 2017 PT. PP TBK ALL RIGHT RESERVED </p>
    @else
    <h1>404</h1>
    Post Not Found
    @endif