
  <section id="home-pt-pp-property" class="">
    <h2 data-aos="fade-down" data-aos-easing="ease-in-cubic" data-aos-delay="200" data-aos-offset="0">
      @if (isset($data['section_11']->title))
        {{$data['section_11']->title}}
      @else 
        No Title
      @endif
    </h2>
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-6 right-border" data-aos="fade-down" data-aos-easing="ease-in-cubic" data-aos-delay="400" data-aos-offset="0">
          <video style="width: 100%" autoplay controls loop>
            <source src="{{ asset('video/loop_vid.mp4') }}" type="video/mp4">
          </video>
          @if (isset($data['section_11']->mediaPost->file))
          <!-- <img src="{{asset('upload/media/'.$data['section_11']->mediaPost->file)}}" class="pt-pp-image" data-aos="fade-down" data-aos-easing="ease-in-cubic" data-aos-delay="400" data-aos-offset="0"> -->
          @else
          <!-- <img src="{{asset('front-end/img/pp.jpg')}}" class="pt-pp-image" data-aos="fade-down" data-aos-easing="ease-in-cubic" data-aos-delay="400" data-aos-offset="0"> -->
          @endif
        </div>
        <div class="col-md-6 " data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="200" data-aos-offset="0">
          @if (isset($data['section_11']->description))
            {!! $data['section_11']->description !!}
          @endif
        </div>
      </div>
      <!-- <div class="row">
        <div class="col-md-6 " data-aos="fade-down" data-aos-easing="ease-in-cubic" data-aos-delay="400" data-aos-offset="0">
          @if (isset($data['section_12']->description))
            {!! $data['section_12']->description !!}
          @endif
        </div>
        <div class="col-md-6 left-border" data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="400" data-aos-offset="0">
          @if (isset($data['section_12']->mediaPost->file))
          <img src="{{asset('upload/media/'.$data['section_12']->mediaPost->file)}}" class="pt-pp-image" data-aos="fade-down" data-aos-easing="ease-in-cubic" data-aos-delay="400" data-aos-offset="0">
          @else
          <img src="{{asset('front-end/img/pp.jpg')}}" class="pt-pp-image" data-aos="fade-down" data-aos-easing="ease-in-cubic" data-aos-delay="400" data-aos-offset="0">
          @endif
        </div>
      </div> -->

    </div>
  </section>