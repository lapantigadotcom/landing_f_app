<nav class="sticky row top">
    <div class="col-md-1">
      <a href="" class="menu-icon-href">
        <img src="{{asset('assets/theme/grandshamaya/img/drawer_icon-01.png')}}" class="menu-icon" >
      </a>
      <ul class="menu-href hidden">
        <li class="menu-href"><a href="home">HOME</a></li>
        <li class="menu-href"><a href="tower">TOWER</a></li>
        <li class="menu-href"><a href="event">EVENT</a></li>
        <li class="menu-href"><a href="gallery">GALLERY</a></li>
        <li class="menu-href"><a href="location">LOCATION</a></li>
        <li class="menu-href"><a href="about">ABOUT US</a></li>
        <li class="menu-href"><a href="contact">CONTACT</a></li>
      </ul>
    </div>
    <style type="text/css">
      .nav-show-first{
        float:left;
        display: inline;
      }
      .search-field{
        display: none;
      }
      .nav-show-first:hover .search-field{
        display: block;
        opacity: 0.6;
      }
      .nav-show-first:hover .search-toggle{
        display: none;
      }
    </style>
    <div class="pull-right">
      <div class="nav-show-first">
        <img src="{{asset('assets/theme/grandshamaya/img/search.png')}}" style="max-width: 48px; width: 100%;" class="search-toggle">
        <input type="text" name="" placeholder="search" class="form-control search-field">
      </div>
      <div class="nav-show-first">
       | English
      </div>
    </div>
  </nav>