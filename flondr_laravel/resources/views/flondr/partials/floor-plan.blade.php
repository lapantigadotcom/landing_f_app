<!-- Generator: Adobe Illustrator 19.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="-90 87.7 841.9 595.3" style="enable-background:new -90 87.7 841.9 595.3;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#FFF0BF;}
	.st1{fill:#fff; fill-opacity:0;}
	.st2{fill:#95B6E0;}
	.st3{fill:#FFFFFF;}
	.st4{fill:#fff; fill-opacity:0;}
	.st5{fill:#fff; fill-opacity:0;stroke:#989898;stroke-width:0.3;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
	.st6{fill:#fff;fill-opacity:0;}
	.st7{fill:#fff; fill-opacity:0;stroke:#767676;stroke-width:0.3;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
	.st8{fill:#989898;}
	.st9{fill:#FFD844;}
	.st10{fill:#F5F5F5;}
	.st11{fill:#D1DA9C;}
	.st12{fill:#fff; fill-opacity:0;}
	.st13{fill:#DBDBDB;}
	.st14{fill:#fff; fill-opacity:0;}
	.st15{fill:#FFD846;}
	.st16{fill:#00404A;}
	.st17{fill:#9AB6DD;}
	.st18{fill:#0C2132;}
	.st19{fill:#fff; fill-opacity:0;stroke:#575756;stroke-width:0.5;stroke-miterlimit:10;}
	.st20{fill:#D1DB9F;}
	.st21{fill:#B58447;}
	.st22{fill:#575756;}
	.st23{font-family:'TimesNewRomanPSMT';}
	.st24{font-size:14.6564px;}
	.st25{font-size:9.2356px;}
	.st26{font-size:15.9942px;}
	.st27{fill:#fff; fill-opacity:0}
	.st1:hover, .st6:hover, .st27:hover{
		fill-opacity: 0.3;
	}
</style>
@include('grdshama.partials.floor-component')
<g>
	<polyline class="st6 floor-trigger" floor-trigger="5" points="462.6,391.1 534.5,391.1 718.3,482.8 718.3,539.5 534.5,539.7 483.7,538.1 430,538.1 	"/>
	<polygon class="st1 floor-trigger" floor-trigger="1" points="357,428.2 357,530.7 381.4,637 538,637.3 538,583.6 438.5,540.1 437.1,429.7 	"/>
	<polygon class="st1 floor-trigger" floor-trigger="2" points="273.9,428.2 275.4,529.3 275.2,672.4 379.8,672.4 381.4,637 355.4,529.3 357,428.2 	"/>
	<polygon class="st27 floor-trigger" floor-trigger="9" points="-44.8,87.7 153.4,87.7 152.9,224 185.8,218.9 182.9,318.9 132.7,318.9 131.3,386 5.3,386 5.3,330.6 
		-7.8,330.6 -27.3,269.1 17.8,226.9 	"/>
	<polygon class="st27 floor-trigger" floor-trigger="4" points="153.4,87.7 288.9,87.7 265.9,207.8 265.9,318.9 182.9,318.9 185.8,218.9 152.9,224 	"/>
	<polygon class="st27 floor-trigger" floor-trigger="7" points="321.3,88.7 324.2,202.5 324.2,318.2 433.5,318.9 433.5,284.1 456.7,284.1 456.7,198.3 456.2,89.6 	
		"/>
	<polygon class="st27 floor-trigger" floor-trigger="6" points="456.7,220.6 552.1,227.9 718.3,274.2 718.3,360.4 433.5,362 433.5,284.1 456.7,284.1 	"/>
	<polygon class="st1 floor-trigger" floor-trigger="3" points="275.2,672.4 169.1,671.7 170.1,538.8 191,534.6 192.3,429.7 273.9,428.2 	"/>
	<polygon class="st1 floor-trigger" floor-trigger="8" points="-3.5,679.6 169.1,682.6 170.1,538.8 191,534.6 192.3,429.7 106.5,428.2 106.5,415.1 5.3,415.1 
		5.3,485.7 -7.4,491.7 	"/>
</g>
</svg>
