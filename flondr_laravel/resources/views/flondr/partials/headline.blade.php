<section id="home-headline">
    <h2 class="title with-styling" data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="200" data-aos-offset="0">
    @if (isset($data['section_3']->title))
      {{strtoupper($data['section_3']->title)}}
    @else
      HEADLINES
    @endif
    </h2>
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-3">
          <ul>
            @if (count($data['section_3']->post) > 0)
              @foreach($data['section_3']->post as $post)
              <li data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="200" data-aos-offset="0">
                <a href="{{ url('/read/'. $post->id) }}" class="modaal-ajax">
                @if (isset($post->title))
                  {{$post->title}}
                @else 
                  No-Title
                @endif
                </a>
                <hr class="half-width">
              </li>
              @endforeach
            @endif
            <li data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="280" data-aos-offset="0">See more</li>
          </ul>
        </div>
        <div class="col-md-8 col-md-offset-1" class="headline-image-placeholder">
          <div class="row owl-carousel" data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="280" data-aos-offset="0" style="max-height: 250px; overflow: hidden;">
            @if (count($data['section_3']->post) > 0)
              <?php
              $postCounter = 0;
              $limit = count($data['section_3']->post) > 10 ? 10 : count($data['section_3']->post);

              ?>
              @foreach($data['section_3']->post()->limit($limit)->get() as $post)
                <div class="" data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="280" style="height: 95px;position: relative; float: left; cursor: zoom-in;margin-right: 15px; overflow: hidden;">
                  @if (isset($post->mediaPost->file))
                  <a href="{{ url('/read/'. $post->id) }}" class="modaal-ajax">
                    <img class="maximaze-img" src="{{asset('upload/media/'.$post->mediaPost->file)}}" style="width: 100%; ">
                    </a>
                  @else
                  <a href="{{ url('/read/'. $post->id) }}" class="modaal-ajax">
                    <img class="maximaze-img" data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="280" src="{{asset('front-end/img/gallery.png')}}" style="height: auto;position: relative; float: left; cursor: zoom-in;margin-right: 15px;">
                    </a>
                  @endif
                </div>
                @if ($postCounter ++ == $limit/2)
                  </div>
                  <div class="row owl-carousel" style="max-height: 250px; overflow: hidden; margin-top: 15px">
                @endif
              @endforeach
            @endif
          </div>
        </div>
      </div>
    </div>
</section>