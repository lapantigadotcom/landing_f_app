<section id="home-about-gallery" class="full-width" style="background:#fff">
  <h1>Experience.</h1>
  <div class="row">
    <div class="col-md-3 col-md-offset-3">
      <div class="row">
        <div class="col-sm-3">
          <img src="{{ asset('assets/theme/grandshamaya/img/experience/logo_experience3-01.png') }}" width="100%">
        </div>
        <div class="col-sm-9">
          <p>A Green Landscape</p>
            <p>to Relaxing Your Mind</p>
            <div class="row">
              <div class="col-sm-6">
                <ul>
                  <li>Thematic Garden</li>
                  <li>Waterfall</li>
                  <li>Chess Garden</li>
                  <li>Giant Hammock</li>
                  <li>Kids Garden</li>
                      </ul>
                    </div>
                    <div class="col-sm-6">
                      <ul>
                  <li>Sky Lounge</li>
                  <li>Sky Void</li>
                  <li>Activity Lounge</li>
                  <li>Bird Corner</li>
                  <li>Zen Garden</li>
                </ul>
              </div>
            </div>
        </div>
      </div>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-3"></div>
  </div>
</section>
  <section id="home-about-gallery" class="full-width" style="background:#fff">
    <div class="col-md-12 experiences-text">
      <h2 class="left italic" style="text-align: center !important;">
      @if (isset($data['section_10']['posts']->title))
        {{ strtoupper($data['section_10']['posts']->title) }}
      @else 
        No Title
      @endif
      </h2>
      <div class="row experience-list" >
          <div class="col-md-4" >
            <img src="{{ asset('assets/theme/grandshamaya/img/experience/logo_experience1-01.png') }}">
            <p>An Exquisite</p>
            <p>Urban Oase</p>
            <div class="row">
            	<div class="col-sm-6">
            		<ul>
            			<li>Infinity Pool</li>
						<li>Warm Pool</li>
						<li>Kids Pool</li>
						<li>Reflection Pool</li>
            		</ul>
            	</div>
            	<div class="col-sm-6">
            		<ul>
						<li>Leasure Pool</li>
						<li>Koi Pond</li>
						<li>Contempory</li>
						<li>Balebengong</li>
					</ul>
            	</div>
            </div>
          </div>
          <div class="col-md-4" >
            <img src="{{ asset('assets/theme/grandshamaya/img/experience/logo_experience2-01.png') }}">
            <p>Luxury Facility in</p>
            <p>Luxury Resort</p>
            <div class="row">
            	<div class="col-sm-6">
            		<ul>
            			<li>Spa & Sauna</li>
						<li>Lounge </li>
						<li>Outdoor GYM</li>
						<li>Barbeque Area</li>
						<li>Pet Care</li>
						<li>Bicycle Parking</li>
						<li>Indoor Playground</li>
            		</ul>
            	</div>
            	<div class="col-sm-6">
            		<ul>
						<li>Sky Jogging Track</li>
						<li>Yoga Deck</li>
						<li>Mini Golf</li>
						<li>Club House</li>
						<li>Multipurpose Hall</li>
						<li>Retail & ATM</li>
						<li>Child Day Care</li>
					</ul>
            	</div>
            </div>
          </div>
          <div class="col-md-4" >
            <img src="{{ asset('assets/theme/grandshamaya/img/experience/logo_experience3-01.png') }}">
            <p>A Green Landscape</p>
            <p>to Relaxing Your Mind</p>
            <div class="row">
            	<div class="col-sm-6">
            		<ul>
            			<li>Thematic Garden</li>
						<li>Waterfall</li>
						<li>Chess Garden</li>
						<li>Giant Hammock</li>
						<li>Kids Garden</li>
            		</ul>
            	</div>
            	<div class="col-sm-6">
            		<ul>
						<li>Sky Lounge</li>
						<li>Sky Void</li>
						<li>Activity Lounge</li>
						<li>Bird Corner</li>
						<li>Zen Garden</li>
					</ul>
            	</div>
            </div>
          </div>
      </div>
    </div>
    <div class="cleardiv">&nbsp;</div>
  </section>
