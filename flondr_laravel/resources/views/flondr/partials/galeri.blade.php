<style type="text/css">
  .owl-prev{
    right: 15px;
  }
  .owl-next{
    left: 15px;
  }
  .owl-prev, .owl-next{
    height: 25px; 
    margin-top: 100px; 
    position: absolute;
  }
</style>
<section id="home-gallery" class="full-width" id="gallery" style="min-height: 60vh">
    <h2 class="with-styling">
      @if (isset($data['section_6']->title))
        {{strtoupper($data['section_6']->title)}}
      @else 
        No Title
      @endif
    </h2>
    <div class="col-md-1">
      <img src="{{ asset('assets/theme/grandshamaya/img/webdesign-18.png') }}" class="owl-prev">
    </div>
    <div class="col-md-10">
      @if (isset($data['section_6']->subportfolio))
        @if (count($data['section_6']->subportfolio) > 0)
          @foreach($data['section_6']->subportfolio as $subportfolio)
            <div class="row owl-carousel" style="max-height: 115px; overflow: hidden; margin-bottom: 15px">
              @if (isset($subportfolio->detailPortfolio))
                @if (count($subportfolio->detailPortfolio) > 0)
                  @foreach($subportfolio->detailPortfolio as $image)
                    @if (isset($image->media->file))
                    <a href="{{asset('upload/media/'. $image->media->file)}}" class="thumbnails gallery maximaze-img-placeholder">
                      <img src="{{asset('upload/media/'. $image->media->file)}}" class="img-fluid" data-aos-duration="1800" data-aos="zoom-in" data-aos-easing="ease-in-sine"  src="{{asset('front-end/img/gallery.png')}}" style="height: auto;position: relative; cursor: zoom-in; float: left; margin-right: 15px; ">
                    </a> 
                    @endif
                  @endforeach
                @endif
              @endif
            </div>
          @endforeach
        @endif
      @endif
    </div>
    <div class="col-md-1">
      <img src="{{ asset('assets/theme/grandshamaya/img/webdesign-17.png') }}" class="owl-next">
    </div>
  </section>