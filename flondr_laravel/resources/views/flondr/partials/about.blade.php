<section id="home-about" class="row" style="min-height:75vh">
    <div class="col-md-8 col-md-offset-2">
      <div class="row">
        <div class="col-md-7">
          @if (isset($data['section_9']->mediaPost->file))
            <!-- <iframe style="width: 100%; min-height: 360px;" src="https://www.youtube.com/embed/Hldq1mkM45U?autoplay=0" frameborder="0" allowfullscreen></iframe> -->
            <img src="{{asset('upload/media/'.$data['section_9']->mediaPost->file)}}" class="image-introduction  " >
          @endif
        </div>
        <div class="col-md-5 ">
          <h2 class="left" style="color: white">
          ABOUT US
            <!-- @if (isset($data['section_9']->title))
              {{strtoupper($data['section_9']->title)}}
            @else 
              No Title
            @endif -->
          </h2>
          <p style=" text-align: justify;">
            <article style="color: white !important;">
            @if ($data['english'])
              {!! $data['section_9']->eng_description !!}
            @else
              {!! $data['section_9']->description !!}
            @endif
            <article>
          </p>
          <button class="readmore">READ MORE</button>
        </div>
      </div>
    </div>
</section>