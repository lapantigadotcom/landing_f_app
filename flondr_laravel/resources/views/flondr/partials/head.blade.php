<link href="{{asset('assets/theme/grandshamaya/style.css')}}" rel="stylesheet" type="text/css"/>
<!-- Bootstrap -->
<link href="{{asset('assets/theme/grandshamaya/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css"/>
<!-- JQuery UI -->
<link href="{{asset('assets/theme/grandshamaya/jquery/css/jquery-ui.min.css')}}" rel="stylesheet" type="text/css"/>

<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>

<link type="text/css" rel="stylesheet" href="{{asset('assets/theme/grandshamaya/plugins/featherlight/featherlight.css')}}" />
  <link type="text/css" rel="stylesheet" href="{{asset('assets/theme/grandshamaya/plugins/featherlight/featherlight.gallery.css')}}" />

<!-- Owl Carousel 2 -->
<link rel="stylesheet" href="{{asset('assets/theme/grandshamaya/owlcarousel/css/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/theme/grandshamaya/owlcarousel/css/owl.theme.default.min.css')}}">
<!-- MaxImage -->
<link rel="stylesheet" href="{{asset('assets/theme/grandshamaya/css/jquery.maximage.css?v=1.2')}}" type="text/css" media="screen" charset="utf-8" />

<link rel="stylesheet" type="text/css" href="{{ asset('assets/theme/grandshamaya/modaal/css/modaal.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/theme/grandshamaya/css/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/theme/grandshamaya/css/slick-theme.css') }}">
  <script src="{{asset('assets/theme/grandshamaya/js/map_gsm.js')}}" type="text/javascript" charset="utf-8"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBLmsbEoXwdEs1YZG7Z7gkSPspLLZJGwM&callback=initMap"
    async defer></script> 
<style type="text/css">
	.slick-dots{
		margin-top: -25px;
		bottom: auto;
	}
	.slick-dots li button{
		border-radius: 20px;
		background-color: rgb(245,245,245);
	}

	.slick-dots li.slick-active button{
		background-color: rgb(150,150,150);
	}
</style>