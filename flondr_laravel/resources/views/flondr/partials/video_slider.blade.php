<style type="text/css">
  video.background { 
    position: absolute;
    top: 50%;
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: 100vh;
    z-index: 0;
    transform: translateX(-50%) translateY(-50%);
    background: url('//demosthenes.info/assets/images/polina.jpg') no-repeat;
    background-size: cover;
    transition: 1s opacity;
    object-fit: fill;
}
.stopfade { 
   opacity: .5;
}

#polina { 
  font-family: Agenda-Light, Agenda Light, Agenda, Arial Narrow, sans-serif;
  font-weight:100; 
  background: rgba(0,0,0,0.3);
  color: white;
  padding: 2rem;
  width: 33%;
  margin:2rem;
  float: right;
  font-size: 1.2rem;
}
h1 {
  font-size: 3rem;
  text-transform: uppercase;
  margin-top: 0;
  letter-spacing: .3rem;
}
#polina button { 
  display: block;
  width: 80%;
  padding: .4rem;
  border: none; 
  margin: 1rem auto; 
  font-size: 1.3rem;
  background: rgba(255,255,255,0.23);
  color: #fff;
  border-radius: 3px; 
  cursor: pointer;
  transition: .3s background;
}
#polina button:hover { 
   background: rgba(0,0,0,0.5);
}
@media screen and (max-width: 500px) { 
  div{width:70%;} 
}
@media screen and (max-device-width: 800px) {
  html { background: url(https://thenewcode.com/assets/images/polina.jpg) #000 no-repeat center center fixed; }
  #bgvid { display: none; }
}
</style>
<video class="background" poster="https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/polina.jpg" id="bgvid" playsinline autoplay muted loop>
  <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
<source src="{{ asset('video/loop_vid.mp4') }}" type="video/mp4">
</video>
<section id="home-introduction">
  <div id="logo">
      <center id="logo">
        <img data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="60" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/logo_white_gsm.png')}}">
        <p data-aos="fade-zoom-in" id="animated-example" data-aos-easing="ease-in-back" data-aos-delay="500" data-aos-offset="0" class="scroll-button" style="color: white; margin-top: 10vh">
        <img data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="60" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/webdesign-23.png')}}" style="width: 20px; display: block; margin-left: 47%" class="bounce">
        please scroll</p>
      </center>
    </div>
</section>
<script type="text/javascript">
  var vid = document.getElementById("bgvid");
var pauseButton = document.querySelector("#polina button");

if (window.matchMedia('(prefers-reduced-motion)').matches) {
    vid.removeAttribute("autoplay");
    vid.pause();
    pauseButton.innerHTML = "Paused";
}

function vidFade() {
  vid.classList.add("stopfade");
}

vid.addEventListener('ended', function()
{
// only functional if "loop" is removed 
vid.pause();
// to capture IE10
vidFade();
}); 


pauseButton.addEventListener("click", function() {
  vid.classList.toggle("stopfade");
  if (vid.paused) {
    vid.play();
    pauseButton.innerHTML = "Pause";
  } else {
    vid.pause();
    pauseButton.innerHTML = "Paused";
  }
})
</script>