<style type="text/css">
  video.background { 
    position: absolute;
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    width: auto;
    height: 100vh;
    z-index: 0;
    transform: translateX(-50%) translateY(-50%);
    background: url('//demosthenes.info/assets/images/polina.jpg') no-repeat;
    background-size: cover;
    transition: 1s opacity;
    object-fit: fill;
}
.stopfade { 
   opacity: .5;
}

#polina { 
  font-family: Agenda-Light, Agenda Light, Agenda, Arial Narrow, sans-serif;
  font-weight:100; 
  background: rgba(0,0,0,0.3);
  color: white;
  padding: 2rem;
  width: 33%;
  margin:2rem;
  float: right;
  font-size: 1.2rem;
}
h1 {
  font-size: 3rem;
  text-transform: uppercase;
  margin-top: 0;
  letter-spacing: .3rem;
}
#polina button { 
  display: block;
  width: 80%;
  padding: .4rem;
  border: none; 
  margin: 1rem auto; 
  font-size: 1.3rem;
  background: rgba(255,255,255,0.23);
  color: #fff;
  border-radius: 3px; 
  cursor: pointer;
  transition: .3s background;
}
#polina button:hover { 
   background: rgba(0,0,0,0.5);
}
@media screen and (max-width: 500px) { 
  div{width:70%;} 
}
@media screen and (max-device-width: 800px) {
  html { background: url(https://thenewcode.com/assets/images/polina.jpg) #000 no-repeat center center fixed; }
  #bgvid { display: none; }
}
</style>
<div class="mainbanner_top">
  <video class="background" poster="https://s3-us-west-2.amazonaws.com/s.cdpn.io/4273/polina.jpg" id="bgvid" playsinline autoplay muted loop>
    <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->
  <source src="{{ asset('video/loop_vid.mp4') }}" type="video/mp4">
  </video>
  	<div class="mainbanner_pp">
	    <div class="col-md-4 col-md-offset-1 " style="padding-left:0px;float:left;padding-top:23.34%;">
	      	<img style="max-height:220px" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="100" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/logo_shamaya%20putih-01.png')}}" alt=""/>
		  </div> 

		  <div class="col-md-4 col-md-offset-1 mainbanner_pp" style="padding-left:0px;float:right;padding-top:23.34%;">
		  <ul >
		  <hr class="hr_mainbanner">
		<li data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="100" data-aos-offset="0"><h3>Luxury Facilities 
		in Luxury Resort</h3></li>
		<hr class="hr_mainbanner">
		<li data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="300" data-aos-offset="0"><h3>A Green Landscape
		to Relaxing Your Mind</h3></li>
		<hr class="hr_mainbanner">
		<li data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="600" data-aos-offset="0"><h3>Luxury Facilities 
		in Luxury Resort</h3></li>
		<hr class="hr_mainbanner">
		</ul> 
		</div>
	</div>
</div>
<script type="text/javascript">
  var vid = document.getElementById("bgvid");
var pauseButton = document.querySelector("#polina button");

if (window.matchMedia('(prefers-reduced-motion)').matches) {
    vid.removeAttribute("autoplay");
    vid.pause();
    pauseButton.innerHTML = "Paused";
}

function vidFade() {
  vid.classList.add("stopfade");
}

vid.addEventListener('ended', function()
{
// only functional if "loop" is removed 
vid.pause();
// to capture IE10
vidFade();
}); 


pauseButton.addEventListener("click", function() {
  vid.classList.toggle("stopfade");
  if (vid.paused) {
    vid.play();
    pauseButton.innerHTML = "Pause";
  } else {
    vid.pause();
    pauseButton.innerHTML = "Paused";
  }
})
</script>

    
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/57e390a10251ff28078cdf59/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>