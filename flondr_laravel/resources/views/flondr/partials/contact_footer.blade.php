
  <section id="home-contact-us">
    <h2 class="with-styling" style="color:#fff">CONTACT US</h2>
    <div class="col-md-12">
      <div class="row">
        <form>
          <div class="col-md-4">
            <div class="form-group " data-aos-duration="1200" data-aos="fade-down" data-aos-easing="ease-in-cubic" >
              <input type="" name="" class="form-control" placeholder="First Name *">
            </div>
            <div class="form-group " data-aos-duration="1400" data-aos="fade-down" data-aos-easing="ease-in-cubic" >
              <input type="" name="" class="form-control" placeholder="Last Name *">
            </div>
            <div class="form-group " data-aos-duration="1600" data-aos="fade-down" data-aos-easing="ease-in-cubic" >
              <input type="" name="" class="form-control" placeholder="Email Address *">
            </div>
            <div class="form-group " data-aos-duration="1800" data-aos="fade-down" data-aos-easing="ease-in-cubic" >
              <input type="" name="" class="form-control" placeholder="Phone Number *">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group " data-aos-duration="2200" data-aos="fade-up" data-aos-easing="ease-in-cubic" >
              <input type="" name="" class="form-control" placeholder="How did you hear about us? *">
            </div>
            <div class="form-group " data-aos-duration="2400" data-aos="fade-down" data-aos-easing="ease-in-cubic" >
              <input type="" name="" class="form-control" placeholder="What are you looking for? *">
            </div>
            <div class="form-group " data-aos-duration="2600" data-aos="fade-up" data-aos-easing="ease-in-cubic" >
              <textarea class="form-control" placeholder="Questions or Comments" style="height: 85px"></textarea>
            </div>
            <div class="form-group " data-aos-duration="2860" data-aos="fade-up" data-aos-easing="ease-in-cubic" >
              <input type="submit" name="" class="form-control col-sm-4 pull-right" style="width: 100px;" value="Send">
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group" data-aos-duration="2800" data-aos="fade-up" data-aos-easing="ease-in-cubic" >
              <button class="form-control contact-button">Contact Our Marketing Executive<i class="fa fa-caret-right"></i></button>
            </div>
            <div class="row contact-list" style="display: none;"  data-aos-duration="2800" data-aos="fade-up" data-aos-easing="ease-in-cubic" >
              <div class="col-md-12" data-aos-duration="2860" data-aos="fade-up" data-aos-easing="ease-in-cubic">
                <div class="contact-list-container">
                  {!!
                  $data['footer']->text
                  !!}
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-12" style="margin-top: 25px;">
      <div class="row">
        <div class="col-md-2" data-aos-duration="2000" data-aos="fade-down" data-aos-easing="ease-in-sine" >
          <img src="{{asset('front-end/img/pp_logo.png')}}" class="footer-logo">
        </div>
        <div class="col-md-10"  data-aos-duration="2200" data-aos="fade-up" data-aos-easing="ease-in-sine"  style="margin-top: 5px; padding-top: 5px">
          <p style="color: white">Marketing Gallery</p>
          <b>GRAND SHAMAYA</b>
          <p style="color: white">Jl. Manyar Kertoarjo 71 Surabaya</p>
          <p style="color: white">(031) 594 79 77</p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12" style="color: white">
          <center>
            <p class="footer-copyright">COPYRIGHT 2017 PT. PP TBK ALL RIGHT RESERVED
            </p>
            <div class="row">
              <hr class="col-md-8 col-md-offset-2"/>
            </div>
            <p class="footer-copyright">
              The Information and image combined here just artistic and subject to change without notifications and maybe requeired by the relevant authorities of developers architect and can't from a part of an offer or contract whilst. Every care has been taken in providing this information, the owner and manager can't be help llable for varition. Modification and substations may be recommended by the company's architect and/or relevant.
            </p>
          </center>
        </div>
      </div>
    </div>
  </section>