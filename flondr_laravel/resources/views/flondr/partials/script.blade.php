    <script src="{{asset('assets/theme/grandshamaya/jquery/js/jquery-1.10.2.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/theme/grandshamaya/jquery/js/jquery-ui.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/theme/grandshamaya/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/theme/grandshamaya/owlcarousel/js/owl.carousel.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/theme/grandshamaya/waypoints/noframework.waypoints.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('assets/theme/grandshamaya/js/jquery.cycle.all.js')}}" type="text/javascript" charset="utf-8"></script>
  <script src="{{asset('assets/theme/grandshamaya/js/jquery.maximage.js')}}" type="text/javascript" charset="utf-8"></script>

  <script src="{{asset('assets/theme/grandshamaya/plugins/featherlight/featherlight.js')}}" type="text/javascript" charset="utf-8"></script>
  <script src="{{asset('assets/theme/grandshamaya/plugins/featherlight/featherlight.gallery.js')}}" type="text/javascript" charset="utf-8"></script>
  <script src="{{ asset('assets/theme/grandshamaya/js/slick.min.js') }}" type="text/javascript" charset="utf-8"></script>
  <script src="{{asset('assets/theme/grandshamaya/modaal/js/modaal.min.js')}}">
    
  </script>
  <script type="text/javascript">
  $('.slick').slick({
    dots:true,
    arrows:true,
    prevArrow: $('.slick-custom-prev'),
    nextArrow: $('.slick-custom-next')
  });
  
  AOS.init({
  duration: 2400,
});

      //modal image pop up//
     $(document).ready(function(){
        $('.gallery').featherlightGallery({
          gallery: {
            fadeIn: 300,
            fadeOut: 300
          },
          openSpeed:    300,
          closeSpeed:   300
        });
        $('.gallery2').featherlightGallery({
          gallery: {
            next: 'next »',
            previous: '« previous'
          },
          variant: 'featherlight-gallery2'
        });
      });

    
    $('.menu-icon-href').click(function(e){
      e.preventDefault();
    });
    function animateElement(elementName){
      $('.' + elementName + '-toggle-init').each(function(i){
        var element = $(this);
        setTimeout(function(){
          element.toggle('puff', 1000);
          element.removeClass(elementName + '-toggle-init');
        }, i * 400);
      });
    }
     

    $(document).ready(function(){
      var owl = $('.owl-carousel');
      owl.owlCarousel({
        items:5,
        lazyLoad:true,
        loop:true,
        margin:10,
        navigation:true,
        navigationText: ["prev","next"]
      });
      $('.owl-next').click(function() {
        owl.trigger('next.owl.carousel');
      }) 
      $('.owl-prev').click(function() {
        owl.trigger('prev.owl.carousel');
      }) 
    });
  </script>

 
 <script type="text/javascript">
  AOS.init({
    duration: 2400,
  });

      //modal image pop up//
     $(document).ready(function(){
        $('.gallery').featherlightGallery({
          gallery: {
            fadeIn: 300,
            fadeOut: 300
          },
          openSpeed:    300,
          closeSpeed:   300
        });
        $('.gallery2').featherlightGallery({
          gallery: {
            next: 'next »',
            previous: '« previous'
          },
          variant: 'featherlight-gallery2'
        });
      });

    // var stickyTop = $('.scrollable-header').position().top;
    var stickyTop = window.innerHeight;
    $(function(){
      // Trigger maximage
      // jQuery('#maximage').maximage();
    });
    function stickyHeader() {
      if( $(window).scrollTop() >= stickyTop) {
        if( !$('.scrollable-header').hasClass('sticky') ) {
          $('.scrollable-header').addClass('sticky');
          $('#home-introduction-container').addClass('home-introduction-container');
        }
      } else {
        if( $('.scrollable-header').hasClass('sticky') ) {
          $('.scrollable-header').removeClass('sticky');
          $('#home-introduction-container').removeClass('home-introduction-container');
        }
      }
    }
    
    function animateElement(elementName){
      $('.' + elementName + '-toggle-init').each(function(i){
        var element = $(this);
        setTimeout(function(){
          element.toggle('puff', 1000);
          element.removeClass(elementName + '-toggle-init');
        }, i * 400);
      });
    }
     
    var selected = 0;
    $('#arrow-trigger-left').click(function(){
      selected--;
    })
    $('#arrow-trigger-right').click(function(){
      selected++;
    })

    function selectFloor(){
      
    }
    var selectedFloor = 0;
    var floorPreviews = [];
    var floorTitles = [
      '1 Bedroom Type A', '1 Bedroom Type B', '1 Bedroom Type C', '1 Bedroom Type D',
      '2 Bedroom Type A', '2 Bedroom Type B', '2 Bedroom Type C',
      '3 Bedroom Type A', '3 Bedroom Type B',
      ];
    var floorDescription = [
      'SGB 75m2', 'SGB 75m2', 'SGB 75m2', 'SGB 75m2',
      'SGA 100m2', 'SGA 100m2', 'SGA 100m2', 
      'SGA 118m2', 'SGA 118m2'
    ];
    var floorFacility = [
      ['Living Room', 'Balcony', 'Pantry', 'Dining Room', 'Bedroom', 'Bathroom'],
      ['Living Room', 'Balcony', 'Pantry', 'Dining Room', 'Bedroom', 'Bathroom'],
      ['Living Room', 'Balcony', 'Pantry', 'Dining Room', 'Bedroom', 'Bathroom'],
      ['Living Room', 'Balcony', 'Pantry', 'Dining Room', 'Bedroom', 'Bathroom'],

      ['Living Room', 'Balcony', 'Pantry', 'Dining Room', 'Master Bedroom','Bedroom(2)', 'Bathroom'],
      ['Living Room', 'Balcony', 'Pantry', 'Dining Room', 'Master Bedroom','Bedroom(2)', 'Bathroom', 'AC Ledge'],
      ['Living Room', 'Balcony', 'Pantry', 'Dining Room', 'Master Bedroom','Bedroom(2)', 'Bathroom', 'AC Ledge'],
      
      ['Private Lift', 'Vestibule', 'Planter','Living Room', 'Balcony', 'Pantry', 'Dining Room', 'Master Bedroom','Bedroom(2)', 'Bathroom', 'AC Ledge', 'Kitchen', 'Maid Bedroom'],
      ['Private Lift', 'Vestibule', 'Planter','Living Room', 'Balcony', 'Pantry', 'Dining Room', 'Master Bedroom','Bedroom(2)', 'Bathroom', 'AC Ledge', 'Kitchen', 'Maid Bedroom'],
      
    ];
    $('.floor-trigger').click(function(){
      selectedFloor = $(this).attr('floor-trigger') -1;
      console.log(selectedFloor+1);
      changeFloor();
    });
    @for ($i = 0; $i<9;$i++)
      floorPreviews.push("{{ asset('assets/theme/grandshamaya/floor-plan/fp'.($i+1).'.png') }}");
    @endfor
    function changeFloor(){
      $('#floor-preview').attr('src', floorPreviews[selectedFloor]);
      $('#floor-title').text(floorTitles[selectedFloor]);
      $('#floor-description').text(floorDescription[selectedFloor]);
      $('#floor-location').attr('src', floorPreviews[selectedFloor]);
      resetFacility();
    }
    resetFacility();
    function resetFacility(){
      $('#floor-facility-1').empty();
      $('#floor-facility-2').empty();
      addFacility();
    }
    function addFacility(){
      var facility = floorFacility[selectedFloor];
      console.log(facility);
      for(var i = 0; i < facility.length/2; i++){
        $('#floor-facility-1').append('<li><img src="{{ url("assets/theme/grandshamaya/img/number/img-") }}'+(i+1)+'.png" class="numbering">'+facility[i]+'</li>');
      }
      for(var i = facility.length%2 == 0? facility.length/2 : facility.length/2 + .5; i < facility.length; i++){
        $('#floor-facility-2').append('<li><img src="{{ url("assets/theme/grandshamaya/img/number/img-") }}'+(i+1)+'.png" class="numbering">'+facility[i]+'</li>');
      }
    }
    
    $('.contact-button').click(function(e){
      if ($('.contact-list').css('display') == 'none')
        $('.contact-list').slideDown();
      else 
        $('.contact-list').slideUp();
      e.preventDefault();
    })

    $('#prev-arrow').click(function(){
      selectedFloor = --selectedFloor < 0 ? floorPreviews.length -1  : selectedFloor;
      changeFloor();
    });
    $('#next-arrow').click(function(){  
      selectedFloor = ++selectedFloor > floorPreviews.length ? 0  : selectedFloor;
      changeFloor();
    });
    $('.place-category').click(function(){
      if ($(this).hasClass('active'))
        $(this).removeClass('active');
      else {
        $('.place-category').removeClass('active');
        $(this).addClass('active');
      }
    })
    $('.map-path-placeholder').css('margin-top',($('#map-image').height() * -1) - 25);
    $('#search-form').on('submit', function(e){
      $('#search-button-trigger').attr('href', "{{ url('post/search?q=') }}" + $("#search-query").val()).click();
      e.preventDefault();
      return false;
    })
    $(document).ready(function(){
      routing();
    })
  </script>