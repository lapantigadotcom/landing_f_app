<style type="text/css">
  
.slick-prev{
    left: 15px;
}

.slick-next{
    right: 15px;
}

.slick-prev, .slick-next{
    background: rgb(200,200,200);
    border-radius: 20px;
    width: 20px;
    height: 20px;
    z-index: 1;
} 
</style>
<section id="home-introduction" style="">
  <div class="col-md-8 col-md-offset-2" id="home-introduction-container">
    <div class="row">
      <div class="col-md-5">
        <h2 data-aos="fade-up" data-aos-delay="300" data-aos-offset="0" class="left italic">
          @if (isset($data['section_1']->title))
            {{ strtoupper($data['section_1']->title)}}
          @else 
            FIRST TOWER
          @endif
        </h2>
        <p data-aos="slide-up" style="font-family: 'HurmeGeometricSans4'; font-weight: lighter;">
          @if ($data['english'])
            {!! $data['section_1']->eng_description !!}
          @else
            {!! $data['section_1']->description !!}
          @endif
        </p>
        <button class="readmore">READ MORE</button>
      </div>
      <div class="col-md-7">
        <div class="row">
          <div class="col-sm-12">
              <div class="row slick">
              @foreach($data['intro_image']->detailPortfolio as $image)
                @if (isset($image->media->file))
                <div style="width: 100%; max-height: 450px;">
                  <img src="{{asset('upload/media/'.$image->media->file)}}" class="image-introduction" data-aos="fade-up" data-aos-delay="300" data-aos-offset="0">
                </div>
                @else 
                <img src="{{asset('front-end/img/first-tower-01.jpg')}}" class="image-introduction" data-aos="fade-up" data-aos-delay="300" data-aos-offset="0">
                @endif
              @endforeach
              </div>
              <div class="row">
                <div class="slick-custom-arrow slick-custom-prev col-sm-1"><img src="{{ asset('assets/theme/grandshamaya/img/arrow-prev-white.png') }}" style="width: 15px;"></div>
                <div class="slick-custom-arrow slick-custom-next col-sm-1 pull-right"><img src="{{ asset('assets/theme/grandshamaya/img/arrow-next-white.png') }}" style="width: 15px;"></div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>