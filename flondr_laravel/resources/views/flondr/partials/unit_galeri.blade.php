
  <section id="home-floor-plan-gallery">
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-5">
          <h2 data-aos="fade-up">
          @if (isset($data['section_2']->subportfolio[1]->title))
            {{$data['section_2']->subportfolio[1]->title}}
          @else
            FLOOR PLAN
          @endif
          </h2>
          @if (isset($data['section_2']->subportfolio[1]->detailportfolio[0]->media->file))
          <img src="{{asset('upload/media/'.$data['section_2']->subportfolio[1]->detailportfolio[0]->media->file)}}" class="image-denah" data-aos="fade-up"
               data-aos-easing="ease-in-back"
               data-aos-delay="160"
               data-aos-offset="0">
          @else
          <img src="{{asset('front-end/img/denah-01.png')}}" class="image-denah" data-aos="fade-up"
               data-aos-easing="ease-in-back"
               data-aos-delay="160"
               data-aos-offset="0">
          @endif
        </div>
        <div class="col-md-7">
          <h2 class="left" data-aos="fade-up">
          @if (isset($data['section_3']->subportfolio[0]->title))
            {{$data['section_3']->subportfolio[0]->title}}
          @else
            UNIT GALLERY
          @endif
          </h2>
          <div class="row floor-plan-gallery">
            @if (isset($data['section_2']->subportfolio[0]->subportfolio))
              @if (count($data['section_2']->subportfolio[0]->subportfolio) > 0)
                @foreach($data['section_2']->subportfolio[0]->subportfolio as $subportfolio)
                <?php
                $title = explode(' ', $subportfolio->title);
                $titleCounter = 0;
                ?>
                <div class="row" data-aos="fade-up" data-aos-easing="linier">
                  <h3>
                  @foreach($title as $t)
                    @if ($titleCounter ++ == 0)
                      <span>{{$t}}</span>
                    @else 
                      {{$t}}
                    @endif
                  @endforeach
                  </h3>
                  <div class="floor-plan-image owl-carousel">
                    @foreach($subportfolio->detailPortfolio as $image)
                      @if (isset($image->media->file))
                      <img src="{{asset('upload/media/'.$image->media->file)}}" class="col-md-4">
                      @endif
                    @endforeach
                  </div>
                </div>
                @endforeach
              @endif
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>