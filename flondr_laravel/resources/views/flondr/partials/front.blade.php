<style type="text/css">
.box {
  width:100%;
  height:100vh;
  display: inline-block;
  vertical-align: top;
  background: url("https://www.w3schools.com/css/trolltunga.jpg") no-repeat;
  background-size: cover;
  position: relative;
}

.overlay {
  position: relative;
}

.overlay:before{
  position: absolute;
  content:" ";
  top:0;
  left:0;
  width:100%;
  height:100%;
  display: block;
  z-index:0;
}
.green:before{
  background: linear-gradient(to bottom, rgba(0, 0, 0, 0), rgba(0, 64, 74, 1));
}


.box * {
    position: relative;
    /* hack */
}

h1 {
  color:white;
}

</style>
<section class="box overlay green">
	<div class="slogan">
		<img src="http://pngimg.com/uploads/car_logo/car_logo_PNG1667.png">
	</div>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<form>
				<input type="text" class="subscription field" name="email" placeholder="EMAIL ADDRESS" required="">
				<input type="submit" class="subscription button" name="" value="Subscribe !">
			</form>
		</div>
	</div>
</section>