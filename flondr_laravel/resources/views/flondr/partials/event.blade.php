<section id="home-event">
    <h2 class="title with-styling" data-aos-duration="600" data-aos="zoom-in" data-aos-easing="ease-in-cubic" data-aos-delay="320">
    @if (isset($data['section_4']->title))
      {{$data['section_4']->title}}
    @else 
      EVENTS
    @endif
    </h2>
    <div class="col-md-10 col-md-offset-1">
      <div class="row">
        @if (isset($data['section_4']->subcategory))
          @if (count($data['section_4']->subcategory) > 0)
            @foreach($data['section_4']->subcategory as $subcategory)
              <div class="col-md-6 " data-aos-duration="600" data-aos="zoom-in" data-aos-easing="ease-in-cubic" data-aos-delay="240">
                <h3>
                  @if (isset($subcategory->title))
                    {{strtoupper($subcategory->title)}}
                  @else 
                    No Title
                  @endif
                </h3>
                <ul class="no-padding">
                  @if (count($subcategory->post) > 0)
                    @foreach ($subcategory->post()->orderBy('id', 'desc')->limit(4)->get() as $post)
                      <li>
                        <a href="{{ url('/read/'. $post->id) }}" class="modaal-ajax">
                        @if (isset($post->title))
                          {{$post->title}}
                        @else 
                          No Title
                        @endif
                        </a>
                      </li>
                    @endforeach
                    <li>See more...</li>
                  @endif
                </ul>
              </div>
            @endforeach
          @endif
        @endif
      </div>
    </div>
</section>