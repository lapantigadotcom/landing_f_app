 
<style type="text/css">
.btn-glyphicon { float:right; padding:8px; background:#ffffff; margin-left:4px; }
.icon-btn { padding: 4px 2px 3px 6px; border-radius:50px; float:left; line-height:2em; margin:1px 1px 10px 1px;}

</style> 
<div id="fasilitas" class="carousel slide" data-ride="carousel" data-interval="2500">
	<!-- Wrapper for slides -->
	<div class="carousel-inner">
		<div class="item active">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<h2>LIVING ROOM</h2>
						<p>1BR: Homogenous Lite, 2BR&3BR: Premium Marble, BEDROOM, Laminated Parquette, BATHROOM, Homogenous Lite, BALCONY, Wood Flooring,</p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<h2>WALL</h2>
						<p>INTERNAL&EXTERNAL, Lightweight brick wall, plaster and paint</p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container-fluid">
				<div class="row">
 					<div class="col-md-12">
						<h2>CEILING</h2>
						<p>Gypsum with shadow line</p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container-fluid">
				<div class="row">
 					<div class="col-md-12">
						<h2>SANITARY</h2>
						<p>Fitting: Grohe or equal, Wares: Kohler or equal</p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container-fluid">
				<div class="row">
 					<div class="col-md-12">
						<h2>GAS</h2>
						<p>Kitchen: Centralized Gas</p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container-fluid">
				<div class="row">
 					<div class="col-md-12">
						<h2>ELECTRICITY</h2>
						<p>PLN: 2300 kwh, Backup: Solar Genset</p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container-fluid">
				<div class="row">
 					<div class="col-md-12">
						<h2>SECURITY SYSTEM</h2>
						<p>CCTV 24 hours installed at main, </p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container-fluid">
				<div class="row">
 					<div class="col-md-12">
						<h2>FACADE</h2>
						<p>Curtain Wall, Ex YKK  with 4% opening, CONCRETE, PreCast Concrete</p>
					</div>
				</div>
			</div>
		</div>
		<div class="item">
			<div class="container-fluid">
				<div class="row">
 					<div class="col-md-12">
						<h2>SMART HOME</h2>
						<p>Automatic Control for Security, (panic button), lighting, AC</p>
					</div>
				</div>
			</div>
		</div>
		<!-- End Item -->
	</div>
	<!-- End Carousel Inner -->


</div>
<a data-target="#fasilitas" data-slide-to="0" class=" active btn icon-btn btn-warning" href="#"><span class="glyphicon btn-glyphicon glyphicon-user img-circle text-warning"></span>LIVING ROOM</a>
<a data-target="#fasilitas" data-slide-to="1"  class=" active btn icon-btn btn-warning" href="#"><span class="glyphicon btn-glyphicon  glyphicon-th img-circle text-warning"></span>WALL</a>
<a data-target="#fasilitas" data-slide-to="2" class=" active btn icon-btn btn-warning" href="#"><span class="glyphicon btn-glyphicon glyphicon-chevron-up img-circle text-warning"></span>CEILING</a>
<a data-target="#fasilitas" data-slide-to="3" class=" active btn icon-btn btn-warning" href="#"><span class="glyphicon btn-glyphicon  glyphicon-globe img-circle text-warning"></span>SANITARY</a>
<a data-target="#fasilitas" data-slide-to="4" class=" active btn icon-btn btn-warning" href="#"><span class="glyphicon btn-glyphicon glyphicon-fire img-circle text-warning"></span>GAS</a>
<a data-target="#fasilitas" data-slide-to="5" class=" active btn icon-btn btn-warning" href="#"><span class="glyphicon btn-glyphicon glyphicon-flash img-circle text-warning"></span>ELECTRICITY</a>
<a data-target="#fasilitas" data-slide-to="6" class=" active btn icon-btn btn-warning" href="#"><span class="glyphicon btn-glyphicon glyphicon-lock img-circle text-warning"></span>SECURITY SYSTEM</a>
<a data-target="#fasilitas" data-slide-to="7"  class=" active btn icon-btn btn-warning" href="#"><span class="glyphicon btn-glyphicon glyphicon-ok-circle img-circle text-warning"></span>FACADE</a>
<a data-target="#fasilitas" data-slide-to="8"  class=" active btn icon-btn btn-warning" href="#"><span class="glyphicon btn-glyphicon glyphicon-home img-circle text-warning"></span>SMART HOME</a>


<script>
$(document).ready(function (ev) {
	$('#fasilitas').on('slide.bs.carousel', function (evt) {
		$('#fasilitas .controls li.active').removeClass('active');
		$('#fasilitas .controls li:eq(' + $(evt.relatedTarget).index() + ')').addClass('active');
	})
});
</script>