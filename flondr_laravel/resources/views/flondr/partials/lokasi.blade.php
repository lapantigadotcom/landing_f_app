<section id="home-location" class="" style="margin-top: -15px">
    <h2 class="title with-styling" data-aos="fade-up" style="margin-top: 0px; padding-top: 25px;">LOCATION</h2> 
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-4">
          <div class="map-button">
            <div>
              [ ]
            </div>
            <div class="map-button-middle">
              Go To Google Maps
            </div>
            <div class="map-button-middle">
              Tower View
            </div>
            <div class="map-button-middle">
              Play Video
            </div>
          </div>
        </div>
        <div class="col-md-3 col-md-offset-5">
          <div class="map-drawer">
            <ul>
              <li class="place-category">
                <span>COMMERCIAL</span>
                <ul>
                  <li class="location-trigger" map-path-action="map-1">Gramedia Expo <span class="pull-right">5 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-2">Tunjungan Plaza <span class="pull-right">5 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-3">Grand City Mall <span class="pull-right">15 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-4">WTC Surabaya <span class="pull-right">15 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-5">Delta Plaza <span class="pull-right">15 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-1">BJ Junction Mall <span class="pull-right">20 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-2">Tunjungan Eletronic Center <span class="pull-right">20 minutes</span></li>
                </ul>
              </li>
              <li class="place-category">
                <span>EDUCATION</span>
                <ul>
                  <li class="location-trigger" map-path-action="map-3">SMA Trimurti Surabaya<span class="pull-right">5 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-4">SMA Petra<span class="pull-right">5 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-5">SMAN 6 Surabaya<span class="pull-right">10 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-1">STIE Urip Sumoharjo<span class="pull-right">10 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-2">IP Surabaya<span class="pull-right">10 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-3">SMAN GIKI 2 Surabaya<span class="pull-right">15 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-4">SMAN 9 Surabaya<span class="pull-right">15 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-5">SMAN 5 Surabaya<span class="pull-right">15 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-1">Airlangga University A<span class="pull-right">20 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-2">Airlangga University B<span class="pull-right">20 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-3">STIKES DR. Soetomo<span class="pull-right">20 minutes</span></li>
                </ul>
              </li>
              <li class="place-category">
                <span>HOSPITAL</span>
                <ul data-aos="fade-up">
                  <li class="location-trigger" map-path-action="map-4">Siloam Hospital<span class="pull-right">10 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-5">Mukti Mulya Hospital<span class="pull-right">15 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-1">Husada Utama Hospital<span class="pull-right">20 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-2">DR. Soetomo Hospital<span class="pull-right">25 minutes</span></li>
                </ul>
              </li>
              <li class="place-category">
                <span>PARK</span>
                <ul data-aos="fade-up">
                  <li class="location-trigger" map-path-action="map-3">Taman Apsari<span class="pull-right">10 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-4">Taman Balai Kota Surabaya<span class="pull-right">15 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-5">Taman Prestasi Surabaya<span class="pull-right">15 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-1">Monumen Kapal Selam<span class="pull-right">20 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-2">BMX & Skatepark<span class="pull-right">20 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-3">Taman Bungkul<span class="pull-right">20 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-4">Kebun Binatang Surabaya<span class="pull-right">20 minutes</span></li>
                </ul>
              </li>
              <li class="place-category">
                <span>TRANSPORTATION</span>
                <ul data-aos="fade-up">
                  <li class="location-trigger" map-path-action="map-5">Trem<span class="pull-right">km</span></li>
                  <li class="location-trigger" map-path-action="map-1">Monorail<span class="pull-right">km</span></li>
                  <li class="location-trigger" map-path-action="map-2">Basuki Rahmat Bus Stop<span class="pull-right">5 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-3">Gubeng Station<span class="pull-right">20 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-4">Pasar Turi Station<span class="pull-right">25 minutes</span></li>
                  <li class="location-trigger" map-path-action="map-5">Juanda International Airport<span class="pull-right">40 minutes</span></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="map">
      <img src="{{ asset('assets/theme/grandshamaya/maps/map-01.png') }}" style="width: 100%; overflow: hidden; display: inline;" id="map-image">
    </div>
    <div class="map-path-placeholder">
      @include('grdshama.partials.map-path')
    </div>
    <!-- <img src="{{asset('front-end/img/location-01.jpg')}}" data-aos-duration="2700" data-aos="fade-down" data-aos-easing="linier" data-aos-delay="240" style="width: 100%"> -->
    <!-- <h3 class="public-places" data-aos="fade-up" data-aos-duration="1400">PUBLIC PLACES</h3>
    <div class="col-md-12">
      <div class="row">
        <div class="col-md-3 ">
          <h4 data-aos="fade-up" data-aos-duration="1000">COMMERCIAL</h4>
          <ul data-aos="fade-down">
            <li class="location-trigger" map-path-action="map-1">Gramedia Expo <span class="pull-right">5 minutes</span></li>
            <li class="location-trigger" map-path-action="map-2">Tunjungan Plaza <span class="pull-right">5 minutes</span></li>
            <li class="location-trigger" map-path-action="map-3">Grand City Mall <span class="pull-right">15 minutes</span></li>
            <li class="location-trigger" map-path-action="map-4">WTC Surabaya <span class="pull-right">15 minutes</span></li>
            <li class="location-trigger" map-path-action="map-5">Delta Plaza <span class="pull-right">15 minutes</span></li>
            <li class="location-trigger" map-path-action="map-1">BJ Junction Mall <span class="pull-right">20 minutes</span></li>
            <li class="location-trigger" map-path-action="map-2">Tunjungan Eletronic Center <span class="pull-right">20 minutes</span></li>
          </ul>
        </div>
        <div class="col-md-3 public-places left-border ">
          <h4 data-aos="fade-up" data-aos-duration="1000">EDUCATION</h4>
          <ul data-aos="fade-down">
            <li class="location-trigger" map-path-action="map-3">SMA Trimurti Surabaya<span class="pull-right">5 minutes</span></li>
            <li class="location-trigger" map-path-action="map-4">SMA Petra<span class="pull-right">5 minutes</span></li>
            <li class="location-trigger" map-path-action="map-5">SMAN 6 Surabaya<span class="pull-right">10 minutes</span></li>
            <li class="location-trigger" map-path-action="map-1">STIE Urip Sumoharjo<span class="pull-right">10 minutes</span></li>
            <li class="location-trigger" map-path-action="map-2">IP Surabaya<span class="pull-right">10 minutes</span></li>
            <li class="location-trigger" map-path-action="map-3">SMAN GIKI 2 Surabaya<span class="pull-right">15 minutes</span></li>
            <li class="location-trigger" map-path-action="map-4">SMAN 9 Surabaya<span class="pull-right">15 minutes</span></li>
            <li class="location-trigger" map-path-action="map-5">SMAN 5 Surabaya<span class="pull-right">15 minutes</span></li>
            <li class="location-trigger" map-path-action="map-1">Airlangga University A<span class="pull-right">20 minutes</span></li>
            <li class="location-trigger" map-path-action="map-2">Airlangga University B<span class="pull-right">20 minutes</span></li>
            <li class="location-trigger" map-path-action="map-3">STIKES DR. Soetomo<span class="pull-right">20 minutes</span></li>
          </ul>
        </div>
        <div class="col-md-3 public-places right-border left-border ">
          <h4 data-aos="fade-up" data-aos-duration="1000">HOSPITAL</h4>
          <ul data-aos="fade-up">
            <li class="location-trigger" map-path-action="map-4">Siloam Hospital<span class="pull-right">10 minutes</span></li>
            <li class="location-trigger" map-path-action="map-5">Mukti Mulya Hospital<span class="pull-right">15 minutes</span></li>
            <li class="location-trigger" map-path-action="map-1">Husada Utama Hospital<span class="pull-right">20 minutes</span></li>
            <li class="location-trigger" map-path-action="map-2">DR. Soetomo Hospital<span class="pull-right">25 minutes</span></li>
          </ul>
          <h4 data-aos="fade-up" data-aos-duration="1000">PARK</h4>
          <ul data-aos="fade-up">
            <li class="location-trigger" map-path-action="map-3">Taman Apsari<span class="pull-right">10 minutes</span></li>
            <li class="location-trigger" map-path-action="map-4">Taman Balai Kota Surabaya<span class="pull-right">15 minutes</span></li>
            <li class="location-trigger" map-path-action="map-5">Taman Prestasi Surabaya<span class="pull-right">15 minutes</span></li>
            <li class="location-trigger" map-path-action="map-1">Monumen Kapal Selam<span class="pull-right">20 minutes</span></li>
            <li class="location-trigger" map-path-action="map-2">BMX & Skatepark<span class="pull-right">20 minutes</span></li>
            <li class="location-trigger" map-path-action="map-3">Taman Bungkul<span class="pull-right">20 minutes</span></li>
            <li class="location-trigger" map-path-action="map-4">Kebun Binatang Surabaya<span class="pull-right">20 minutes</span></li>
          </ul>
        </div>
        <div class="col-md-3 public-places ">
          <h4 data-aos="fade-up" data-aos-duration="1000">TRANSPORTATION</h4>
          <ul data-aos="fade-up">
            <li class="location-trigger" map-path-action="map-5">Trem<span class="pull-right">km</span></li>
            <li class="location-trigger" map-path-action="map-1">Monorail<span class="pull-right">km</span></li>
            <li class="location-trigger" map-path-action="map-2">Basuki Rahmat Bus Stop<span class="pull-right">5 minutes</span></li>
            <li class="location-trigger" map-path-action="map-3">Gubeng Station<span class="pull-right">20 minutes</span></li>
            <li class="location-trigger" map-path-action="map-4">Pasar Turi Station<span class="pull-right">25 minutes</span></li>
            <li class="location-trigger" map-path-action="map-5">Juanda International Airport<span class="pull-right">40 minutes</span></li>
          </ul>
        </div>
      </div>
    </div> -->
</section>