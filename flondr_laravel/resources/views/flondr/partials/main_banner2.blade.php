
 <div class="mainbanner_top">
  <div class="mainbanner_pp">
    <div class="col-md-4 col-md-offset-1 " style="padding-left:0px;float:left;padding-top:23.34%;">
      <img style="max-height:220px" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="100" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/logo_shamaya%20putih-01.png')}}" alt=""/>
 </div> 

<div class="col-md-4 col-md-offset-1 mainbanner_pp" style="padding-left:0px;float:right;padding-top:23.34%;">
 <ul >
  <hr class="hr_mainbanner">
<li data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="100" data-aos-offset="0"><h3>Luxury Facilities 
in Luxury Resort</h3></li>
<hr class="hr_mainbanner">
<li data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="300" data-aos-offset="0"><h3>A Green Landscape
to Relaxing Your Mind</h3></li>
<hr class="hr_mainbanner">
<li data-aos="fade-up" data-aos-easing="ease-in-cubic" data-aos-delay="600" data-aos-offset="0"><h3>Luxury Facilities 
in Luxury Resort</h3></li>
<hr class="hr_mainbanner">
</ul> 
</div>

</div>
</div>

    
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/57e390a10251ff28078cdf59/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>