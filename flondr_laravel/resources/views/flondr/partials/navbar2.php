<nav class="scrollable-header">
  <div class="col-md-1"><a href="home">HOME</a></div>
  <div class="col-md-1"><a href="tower">TOWER</a></div>
  <div class="col-md-1"><a href="location">LOCATION</a></div>
  <div class="col-md-1"><a href="layout">layout unit</a></div>
  <div class="col-md-4"><img  data-aos="fade-up" data-aos-delay="300" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/logo_grandshamaya_new.png')}}" class=""></div>
  <div class="col-md-1"><a href="gallery">GALLERY</a></div>
  <div class="col-md-1"><a href="about">ABOUT US</a></div>
  <div class="col-md-1"><a href="experience">EXPERIENCE</a></div>
  <div class="col-md-1"><a href="contact">CONTACT</a></div>
</nav>