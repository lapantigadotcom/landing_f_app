<div id="ppcloud" class="ppcloud" style="margin-top: 0px; padding-top: 0px; height:100% ; margin:30px 0 0 0">
	<div class="col-md-12 leaf_pp">
      <div class="row">
      	<div class="col-md-7" style="padding-left:0px">

	      <img style="float:left" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="30" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/pp_cloud_img1-01.png')}}" alt=""/>
	  	</div>
	  	<div class="col-md-5"> 
	  		<div class="col-md-7 ">
	  		<ul><img style="max-height:70px" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="60" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/pp_visi-01.png')}}" alt=""/>
			</ul>
			<li><span style="float:left;padding-right:5px"><img style="max-height:70px" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="60" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/icon_pp/icon_visi-01.png')}}" alt=""/></span>
				Menjadi perusahaan pengembang nasional yang terkemuka dan berkelanjutan serta berdaya saing global
			</li>
			<ul><img style="max-height:70px" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="60" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/pp_misi-01.png')}}" alt=""/>
			</ul>
			<li><span style="float:left;padding-right:5px"><img style="max-height:70px" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="60" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/icon_pp/icon_a-01.png')}}" alt=""/></span>
			<p>Mengembangkan produk retail dan properti yang unggul serta inovatif untuk memberikan kenyamanan bagi konsumen.</p></li>
			<li><span style="float:left;padding-right:5px"><img style="max-height:70px" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="60" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/icon_pp/icon_b-01.png')}}" alt=""/></span>
			<p style="">Berkomitmen terhadap lingkungan yang sehat</p><br></li>
			<li><span style="float:left;padding-right:5px"><img style="max-height:70px" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="60" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/icon_pp/icon_c-01.png')}}" alt=""/></span>
			<p>Meningkatkan kontribusi kepada perusahaan induk dengan mengembangkan dan menyelaraskan strategi korporasi.</p></li>
			<li><span style="float:left;padding-right:5px"><img style="max-height:70px" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="60" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/icon_pp/icon_d-01.png')}}" alt=""/></span>
			<p>Menjalin kemitraan strategis dengan mitra kerja.</p><br></li>
			<li><span style="float:left;padding-right:5px"><img style="max-height:70px" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-delay="60" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/icon_pp/icon_e-01.png')}}" alt=""/></span>
			<p>Mewujudkan sumber daya manusia unggul dengan memperhatikan peningkatan kesejahteraan karyawan.</p></li>
			</div>
	  	</div>
	       </div>
	 </div>
 </div>