 <script src="{{asset('assets/theme/grandshamaya/js/map_gsm.js')}}"></script>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBBLmsbEoXwdEs1YZG7Z7gkSPspLLZJGwM&callback=initMap"
    async defer></script> 
    <style type="text/css">
#map_gsm {
        position: fixed;
  height:500px;  
  width:100%; 
  top: 50%;
  left:0;
  right:0;
  margin-left:auto;
  margin-right:auto;
  margin-top: 1px;
  margin-top: 0.1px;
  background: #000;
       }
      
.floating-panel {
    position: absolute;
    margin-top: 25px;
    z-index: 5;
    background-color: #fff;
    padding: 5px;
    border: 1px solid #999;
    text-align: center;
    font-family: 'Luxia-Display';
    line-height: 30px;
    right: 15px;
}

    </style>
    <div id="row" >
         <div id="col-md-12" >
    	<div id="peta_panduan">
    		<div class="floating-panel"  data-aos="fade-down" data-aos-easing="linier" data-aos-delay="12000">
    			<b>Start: </b>
    			<select id="start">
    				<option value="-7.2695394,112.7396138">Gramedia Expo</option>
    				<option value="-7.3788798,112.7851004">Juanda Airport</option>
    				<option value="-7.2722778,112.7558453">Unair Kampus B</option>
    				<option value="-7.3504929,112.7227216">Terminal Bungurasih</option>
    				<option value="-7.3467622,112.7253292">Bundaran Waru</option>
    				<option value="-7.2695394,112.7396138">SMA Trimurti</option>
    				<option value="-7.2163623,112.6493951">Osowilangun</option>
    				<option value="-7.2285215,112.6291099">Benowo</option>
    			</select>
    			<b>End: </b>
    			<select id="end">
    				<option value="-7.267337, 112.743038">Grandshamaya</option>
    			</select>
    		</div>
             <div id="col-md-12" >
    		<div id="map_gsm"></div></div>
    	</div>
    </div>		</div>  
    <div class="clear"></div>