<nav class="scrollable-header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 logo">
        <img  data-aos="fade-up" data-aos-delay="300" data-aos-offset="0" src="{{asset('assets/theme/grandshamaya/img/logo_grandshamaya_new.png')}}" class="">
      </div>
      <div class="col-md-8 col-md-offset-2">
        <div class="navbar-placeholder"><a href="#home">HOME</a></div>
        <div class="navbar-placeholder"><a href="#home-introduction">TOWER</a></div>
        <div class="navbar-placeholder"><a href="#home-floor-plan-gallery">LAYOUT UNIT</a></div>
        <div class="navbar-placeholder"><a href="#home-gallery">GALLERY</a></div>
        <div class="navbar-placeholder"><a href="#home-about">EVENTS</a></div>
        <div class="navbar-placeholder"><a href="#home-about-gallery">EXPERIENCE</a></div>
        <div class="navbar-placeholder"><a href="#home-contact-us">CONTACT US</a></div>
      </div>
    </div>
  </div>
</nav>