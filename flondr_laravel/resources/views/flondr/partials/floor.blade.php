<section id="home-floor-plan-gallery">
  <div class="col-md-12">
      <h2 data-aos="fade-up" style="text-align: center;" class="with-styling">
        LAYOUT UNIT
      </h2>
    <div class="row">
      <div class="col-md-5">
        <div class="row">
          
          <div class="col-md-12">
              @include('grdshama.partials.floor-plan')
              @include('grdshama.partials.fasilitas')
          </div>
        </div>
        <!-- <img src="{{ asset('assets/theme/grandshamaya/img/floor_plan/floor_plan.png') }}" style="width: 100%"> -->
        
      </div>
      <div class="col-md-2">
        <img src="{{ asset('img/floor-separator.png') }}" style="width: 30px; margin-left: 45%;">
      </div>
      <div class="col-md-5">
        <div class="row">
          <div class="col-md-12">
            <div class="col-sm-1" id="arrow-trigger-left">
              <img src="{{ asset('assets/theme/grandshamaya/img/webdesign-18.png') }}" style="width: 70%" id="prev-arrow">
            </div>
            <div class="col-sm-10" >
              <center><img src="{{ asset('assets/theme/grandshamaya/floor-plan/fp1.png') }}" style="width: 70%" id="floor-preview"></center>
            </div>  
            <div class="col-sm-1" id="arrow-trigger-right">
              <img src="{{ asset('assets/theme/grandshamaya/img/webdesign-17.png') }}" style="width: 70%" id="next-arrow">
            </div>
          </div>
        </div>
        <p class="floor-title" id="floor-title">1 Bedroom Type A</p>
        <p class="floor-description" id="floor-description">SGA 118m2</p>
        <div class="row"></div>
          <div class="col-md-3"></div>
          <div class="col-md-8 col-md-offset-1">
            <div class="row" id="floor-feature">
              <ul class="col-md-6 floor-facility" id="floor-facility-1">
                
              </ul>
              <ul class="col-md-6 floor-facility" id="floor-facility-2">
                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</section>