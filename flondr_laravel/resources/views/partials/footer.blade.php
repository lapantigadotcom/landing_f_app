<!-- jQuery 2.0.2 
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script> -->
        <script type="text/javascript" src="{{ asset('js/adminlte/jquery.min.js') }}"></script>
        <!-- Bootstrap -->
        <script type="text/javascript" src="{{ asset('js/adminlte/bootstrap.min.js') }}"></script>
        
        <!-- AdminLTE App -->
        <script type="text/javascript" src="{{ asset('js/adminlte/app.min.js') }}"></script>
         <script src="{{asset('assets/theme/metronic/assets/scripts/app.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/theme/metronic/assets/scripts/index.js')}}" type="text/javascript"></script>
   <script src="{{asset('assets/theme/metronic/assets/scripts/tasks.js')}}" type="text/javascript"></script>     
		<script type="text/javascript">
		var gcaptchaVerify = 0; 
                function reCaptchaVerify(response){gcaptchaVerify = 1;} 
                var onloadCallback = function() { 
                        grecaptcha.render('recaptcha', { 'sitekey' : '6LdNugoUAAAAAKer7jMhmFx8PCzQjBGESYtRi8Kk', 'callback': reCaptchaVerify }); 
                }; 
                $(".gcaptcha").on('submit', function(e){ 
                        if (gcaptchaVerify==0)e.preventDefault();
                })
		</script>
		<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
        @yield('custom-footer')