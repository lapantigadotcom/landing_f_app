                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                  <!-- Sidebar user panel -->
                  <div class="user-panel">
                    <div class="pull-left image">
                      <img src="{{asset('assets/theme/flondr/images/logo_flondr_beta.png')}}" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                      <p>{!! Auth::user()->name !!}</p>

                      <a href="#"><i class="fa fa-circle text-success"></i>{!! Auth::user()->email !!}</a>
                    </div>
                  </div>
                  <!-- search form -->
                  <form action="#" method="get" class="sidebar-form">
                    <div class="input-group">
                      <input type="text" name="q" class="form-control" placeholder="Search..."/>
                      <span class="input-group-btn">
                        <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                      </span>
                    </div>
                  </form>
                  <!-- /.search form -->
                  <!-- sidebar menu: : style can be found in sidebar.less -->
                  <ul class="sidebar-menu">
                    <li class="">
                      <a href="{!! url('vrs-admin') !!}">
                        <i class="icon-home"></i> &nbsp;
                        <span>Dashboard</span>

                      </a>
                    </li>
                    <li class="treeview">
                     <a href="javascript:;">
                      <i class="icon-location-arrow"></i> &nbsp;
                      <span class="title">View Flondr FrontEnd</span>
                      <span class="arrow "></span>
                    </a>
                    <ul class="treeview-menu">
                      <li >
                       <a href="{{ url('/') }}" target="_blank">
                         Lihat Landing Page</a>
                       </li>
                       <li >
                         <a href="{{ url('/grandshamaya/kategori=27') }}" target="_blank">
                           Lihat Event News</a>
                         </li>
                       </ul>
                     </li>
                     <li class="treeview">
                      <a href="javascript:void(0)">
                        <i class="icon-pencil"></i> &nbsp;
                        <span class="title">Event & News</span>
                        <span class="arrow "></span>  
                      </a>
                      <ul class="treeview-menu">
                        <li >
                         <a href="{{ url('vrs-admin/post') }}">
                           Lihat Semua Artikel</a>
                         </li>
                         <li >
                           <a href="{{ url('vrs-admin/post/create') }}">
                             Add Artikel</a>
                           </li>
                           <li >
                             <a href="{!! route('category.index') !!}">
                               Kategori</a>
                             </li><li >
                             <a href="{!! route('tag.index') !!}">
                               Tag</a>
                             </li>
                           </ul>
                         </li>

                         <li class="treeview">
                           <a href="javascript:;">
                             <i class="icon-picture"></i> &nbsp;
                             <span class="title">Media</span>
                             <span class="arrow "></span>
                           </a>
                           <ul class="treeview-menu">
                            <li >
                             <a href="{!! route('media.index') !!}">
                               Library</a>
                             </li>
                             <li >
                               <a href="{!! route('portfolio.index') !!}">
                                 Media List</a>
                               </li>
                             </ul>
                           </li>


                           <li class="treeview">
                             <a href="javascript:;">
                               <i class="icon-camera"></i> &nbsp;
                               <span class="title">Gallery</span>
                               <span class="arrow "></span>
                             </a>
                             <ul class="treeview-menu">
                              <li >
                               <a href="{!! route('gerai.index') !!}">
                                 Gallery Management</a>
                               </li>
                             </ul>
                           </li>


                           <li class="treeview">
                             <a href="javascript:;">
                               <i class="icon-cloud"></i> &nbsp;
                               <span class="title">Inbox</span>
                               <span class="arrow "></span>
                             </a>
                             <ul class="treeview-menu">
                              <li >
                               <a href="{{ url('vrs-admin/aduan') }}">
                                 Contact Us</a>
                               </li>
                             </ul>
                           </li>
                           <!-- START TESTIMONI -->
                           <li class="treeview">
                             <a href="javascript:void(0)">
                               <i class="icon-check"></i> 
                               <span class="title">Testimoni</span>
                               <span class="arrow "></span>
                             </a>
                             <ul class="treeview-menu">
                              <li >
                                <a href="{!! route('testi.index') !!}">
                                 Semua Testimoni</a>
                               </li>
                               <li >
                                 <a href="{!! route('testi.create') !!}">
                                   Tambah Baru</a>
                                 </li>
                               </ul>
                             </li>
                             
                             <li class="treeview">
                              <a href="javascript:void(0)">
                               <i class="icon-gear"></i> &nbsp;
                               <span>Pengaturan</span>
                               <i class="fa fa-angle-left pull-right"></i>
                             </a>
                             <ul class="treeview-menu">
                              <li >
                               <a href="{!! url('vrs-admin/brosur') !!}">
                                 Subscription Link</a>
                               </li>
                               <li >
                                 <a href="{!! route('general.index') !!}">
                                   Umum</a>
                                 </li>
                                 <li >
                                   <a href="{!! route('contact.edit',[2]) !!}">
                                     Kontak</a>
                                   </li>
                                   <li >
                                     <a href="{!! route('dailycp.edit',[1]) !!}">
                                       Daily CP</a>
                                     </li>
                                   </ul>
                                 </li>

                               </ul>
                             </section>
                <!-- /.sidebar -->