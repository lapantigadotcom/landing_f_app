@extends('theme.korlantas.layout.index')

@section('content')
<div class="row" style="min-height:70%;">
  <div class="col-md-12">
    <div class="main-banner six">
      <div class="container">
        <h2><span>Reset Password</span></h2>
      </div>
    </div>
    <div class="breadcrumb">
      <div class="container">
        <ul class="list-unstyled list-inline">
          <li><a href="{!! URL::to('/') !!}">Home</a></li>
          <li><a href="javascript:void(0);">Reset Password</a></li>
        </ul>
      </div>
    </div>
    <div class="main-container">    
      <div class="row">
        <div class="col-md-4 col-md-offset-4">            
          <div class="login-box-body">
            <form role="form" method="POST" action="{{ url('/doResetPass') }}">
              @include('page.partials.notification')
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group has-feedback">
                  {!! Form::text('email',old('email'), array('class' => 'form-control', 'placeholder' => 'email')) !!}
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              </div>
              <div class="row">
                <center>
                <button type="submit" class="btn btn-success">Reset</button>
                </center><!-- /.col -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/korlantas/plugins/pretty-photo/css/prettyPhoto.css') }}">

@endsection

@section('custom-footer')
<script type="text/javascript" src="{{ asset('theme/korlantas/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
   $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

 });
</script>
@endsection