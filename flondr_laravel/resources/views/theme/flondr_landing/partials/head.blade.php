 <link href="{{ asset('assets/theme/flondr/css/bootstrap.css')}}" rel='stylesheet' type='text/css' />
<link rel="shortcut icon" href="{{ asset('assets/theme/flondr/favicon/favicon-16x16.png')}}" />
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/theme/flondr/favicon//apple-touch-icon.png')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/theme/flondr/favicon/favicon-32x32.png')}}">
<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/theme/flondr/favicon/favicon-16x16.png')}}">
<link rel="manifest" href="{{ asset('assets/theme/flondr/favicon/manifest.json')}}">
<link rel="mask-icon" href="{{ asset('assets/theme/flondr/favicon/safari-pinned-tab.svg')}}" color="#5bbad5">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script src="{{ asset('assets/theme/flondr/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/theme/flondr/js/move-top.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/theme/flondr/js/easing.js')}}"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){   
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<link href="{{ asset('assets/theme/flondr/css/style.css')}}" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1">
 <meta name="og:site_name" content="">
<meta name="copyright" content="PT. Gatam Perkasa Media">

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
<script>
	$(function() {
		var pull    = $('#pull');
		menu    = $('nav ul');
		menuHeight  = menu.height();
		$(pull).on('click', function(e) {
			e.preventDefault();
			menu.slideToggle();
		});
		$(window).resize(function(){
			var w = $(window).width();
			if(w > 320 && menu.is(':hidden')) {
				menu.removeAttr('style');
			}
		});
	});
</script>
@yield('custom-head')