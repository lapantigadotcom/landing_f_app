<div class="header-section">
  <div id="home" class="header">
    <div class="container">
      <div class="top-header">
        <div class="logo">
          <a href="#"><img src="{{asset('assets/theme/flondr/images/logo_flondr.png')}}" title="logo" /></a>
          </div>
        <nav class="top-nav">
          <ul class="top-nav">
            <li class="active"><a href="#home" class="scroll">Home </a></li>
            <li class="page-scroll"><a href="#fea" class="scroll">Features</a></li>
            <li class="page-scroll"><a href="#about" class="scroll">About </a></li>
            <li class="contatct-active" ><a href="#contact" class="scroll">Contact</a></li>
            <li class="contatct-active"  ><a href="#fb" class="scroll"><i class="fa fa-instagram fa-2x"></i></a></li>
            <li class="contatct-active"  ><a href="#fb" class="scroll"><i class="fa fa-twitter-square fa-2x"></i></a></li>
            <li class="contatct-active"  ><a href="#fb" class="scroll"><i class="fa fa-facebook-square fa-2x"></i></a></li>
            <li class="contatct-active"  ><a href="#fb" class="scroll"><i class="fa fa-pinterest-square fa-2x"></i></a></li>
          </ul>
          <a href="#" id="pull"><img src="images/nav-icon.png" title="menu" /></a>
        </nav>
        <div class="clearfix"> </div>
      </div>
    </div>
  </div>