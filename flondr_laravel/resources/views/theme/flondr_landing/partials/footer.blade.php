 			<div class="footer">
        <div class="container">
          <div class="footer-grids">
            <div class="col-md-12 footer-grid about-info">
              <img class="img-responsive" src="{{asset('assets/theme/flondr/images/logo_flondr_beta.png')}}" style="max-width: 75px" title="logo flondr" />
            </div>
            <div class="clearfix"> </div>
            <script type="text/javascript">
              $(document).ready(function() {
                 $().UItoTop({ easingType: 'easeOutQuart' });
              });
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-82056680-1', 'auto');
              ga('send', 'pageview');
            </script>
            <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
          </div>
        </div>
      </div>
      <link href="{{ asset('assets/theme/flondr/css/popuo-box.css')}}" rel="stylesheet" type="text/css" media="all"/>
      <script src="{{ asset('assets/theme/flondr/js/jquery.magnific-popup.js')}}" type="text/javascript"></script>
      <script src="{{ asset('assets/theme/flondr/js/analytic_flondr.js')}}" type="text/javascript"></script>
      @yield('custom-footer')
