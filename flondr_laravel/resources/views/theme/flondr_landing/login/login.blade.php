@extends('theme.kess.layout.index')

@section('content')
<div class="row" style="min-height:70%;">
  <div class="col-md-12">
    <div class="main-banner six" style="background:transparent url( {{ asset('theme/enagic/images/header-login.jpg') }} ) repeat scroll center top">
     <div class="container">
        <h2>&nbsp;</h2>
      </div>
    </div>
    <div class="breadcrumb">
      <div class="container">
        <ul class="list-unstyled list-inline">
          <li><a href="javascript:void(0);">Login</a></li>
        </ul>
      </div>
    </div>
    <div class="main-container">    
      <div class="row">
        <div class="col-md-4 col-md-offset-4">            
          <div class="login-box-body">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
              <strong>Whoops!</strong> There were some problems with your input.<br><br>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            <form role="form" method="POST" action="{{ url('/user/login') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group has-feedback">
                  {!! Form::text('email',old('email'), array('class' => 'form-control', 'placeholder' => 'email')) !!}
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                  {!! Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) !!}
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="row">
                <div class="col-xs-8">    
                  <div class="checkbox icheck">
                    <label>
                      <input type="checkbox" value="1" name="remember_me" > <span style="color:black;">Ingat Saya</span>
                    </label>
                  </div>                        
                </div><!-- /.col -->
                <div class="col-xs-4">
                  <button type="submit" class="btn btn-success">Sign In</button>
                </div><!-- /.col -->
              </div>
            </form>
            <div class="spacer-block"></div>
            <center><a href="{{ route('home.resetPass') }}" class="btn btn-error">Reset Password</a></center>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/kess/plugins/pretty-photo/css/prettyPhoto.css') }}">

@endsection

@section('custom-footer')
<script type="text/javascript" src="{{ asset('theme/kess/plugins/pretty-photo/js/jquery.prettyPhoto.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function(){
   $(".gallery a[rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'light_square', autoplay_slideshow: false});

 });
</script>
@endsection