    <div class="text-center headlinebarFix">ABOUT
    </div>

    <div id="about" class="getintouch">
      <div class="container">
       <div class="clearfix"> </div>
       <div class="section-head text-center" >
        <h3 style="color:#fff"><span class="frist"> </span>Flondr | Connecting Places<span class="second"> </span></h3>
        <p style="color:#fff">Flondr is the answer of everything you need in your daily life. We provide information about many type of business by giving you the address, phone number, working hours, direction, social media (if applicable).
          You don’t have to wonder when the stores open or closed or false direction. Flondr is the local guide to finding the perfect place to eat, shop, doing business, and play.
          Need Help? Contact Our Email : support@flondr.com</p>
        </div>
        <div class="col-md-12 getintouch-center" id="contact">
          <div class="contact-form col-md-12">
           @include('theme.flondr_landing.layout.form_contact_footer')
        </div>
        <ul class="footer-social-icons col-md-2 text-center">
          <div class="clearfix"> </div>
        </ul>
      </div>

    </div>
  </div>

</div>