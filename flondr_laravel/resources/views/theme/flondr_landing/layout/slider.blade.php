<div  id="top" class="callbacks_container">
  <ul class="rslides" id="slider4">
    <li style="min-height:650px">
     <div class="caption text-center">
      <div class="slide-text-info">
        <h1><span>Flondr | </span>Connecting Places </h1>
        <h2>Find interesting places from your mobile</h2>
        <div class="slide-text">
          <ul style="padding-left:0 ; margin-left:0">
            <li><span> </span>Provide information about many type of bussines </li>
            <li><span> </span>Flondr is the answer of everything you need in your daily life</li>
            <li><span> </span>Local guide to finding the perfect place</li>
          </ul>
          <ul style="padding-left:0 ; margin-left:0">
            <a class="view" href="#"> <img style="width:170px; padding-top:50px" src="{{ asset('assets/theme/flondr/images/app_store.png')}}" /> </a>
            <a class="view" href="https://play.google.com/store/apps/details?id=connecting.places.flondr" target="_blank"><img style="width:174px; padding-top:50px" src="{{ asset('assets/theme/flondr/images/plasytore_icon.png')}}" /> </a>
          </ul>
        </div>


      </div>
    </div>
  </li>

</ul>
</div>
<div class="clearfix"> </div>
<div class="divice-demo ">
  <img src="{{ asset('assets/theme/flondr/images/flondr-slider-front.png')}}"/>
</div>
</div>