    <div class="text-center headlinebarFix">FEATURES
    </div>

    <div id="fea" class="features">
      <div class="container">
        <div class="clearfix"> </div>
        <div class="features-grids">
          <div class="col-md-4 features-grid" style="text-align: right;">
            <div class="features-grid-info">
              <div class="col-md-2 features-icon">
              </div>
              <div class="col-md-10 features-info">
                <h4>Business Places</h4>
                <p>We provide information about many type of business by giving you the address, phone, working hours, direction etc</p>
                <hr class="line_bottom">
              </div>

            </div>
            <div class="features-grid-info">
              <div class="col-md-2 features-icon">

              </div>
              <div class="col-md-10 features-info">
                <h4>Office Hour</h4>
                <p>With Flondr, You can easily know open / closed time for any business place</p>
                <hr class="line_bottom">
              </div>

            </div>
            <div class="features-grid-info">
              <div class="col-md-2 features-icon">
              </div>
              <div class="col-md-10 features-info">
                <h4>Promotion</h4>
                <p>Outlet / store with promotion price (coming soon)</p>
              </div>

            </div>

          </div> 
          <div class="col-md-4 features-grid">
            <div class="big-divice">
              <center>
                <img class="img-responsive img-center" src="{{ asset('assets/theme/flondr/images/flondr_mockup_playstore.png')}}" style="max-width:295px;padding-left: 18px" title="features-demo" /></center>
              </div>
            </div> 
            <div class="col-md-4 features-grid">
              <div class="features-grid-info">
                <div class="col-md-2 features-icon1">
                </div>
                <div class="col-md-10 features-info">
                  <h4>Write Review</h4>
                  <p>add a review about business place You Had visited. It will helping other people to knowing more information</p>
                  <hr class="line_bottom">
                </div>


              </div>

              <div class="features-grid-info">
                <div class="col-md-2 features-icon1">
                </div>
                <div class="col-md-10 features-info">
                  <h4>Get Location</h4>
                  <p>So easy to get direction to your favourite outlets</p>
                  <hr class="line_bottom">
                </div>          
              </div>
              <div class="features-grid-info">
                <div class="col-md-2 features-icon1">
                </div>
                <div class="col-md-10 features-info">
                  <h4>Image Upload</h4>
                  <p>Add a photo to exisiting business place, about how they look & serve</p>
                </div>
                <div class="clearfix"> </div>
              </div>

            </div> 
            <div class="clearfix"> </div>
          </div>
        </div>