<div id="about" class="team">
              <div class="container">
                <div class="section-head text-center">
                  <h3><span class="frist"> </span>PLACES CATEGORY<span class="second"> </span></h3>
                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta.</p>
                </div>
              </div>
              <div class="team-members">
                <div class="container">
                  <div class="col-md-3 team-member">
                    <div class="team-member-info">
                      <img class="member-pic" src="{{ asset('assets/theme/flondr/images/food-cate.jpg')}}" title="name" />
                      <h5><a href="#">Food & Beverage</a></h5>
                      <span>Resto, Cafe, Bars, etc</span>
                      <label class="team-member-caption text-center">
                        <p>sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem.</p>
                        <ul>

                          <div class="clearfix"> </div>
                        </ul>
                      </label>
                    </div>
                  </div> 
                  <div class="col-md-3 team-member">
                    <div class="team-member-info">
                      <img class="member-pic" src="{{ asset('assets/theme/flondr/images/auto-cate.jpg')}}" title="name" />
                      <h5><a href="#">Automotive</a></h5>
                      <span>Workshop, sparepart, etc</span>
                      <label class="team-member-caption text-center">
                        <p>sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem.</p>
                        <ul>

                          <div class="clearfix"> </div>
                        </ul>
                      </label>
                    </div>
                  </div> 
                  <div class="col-md-3 team-member">
                    <div class="team-member-info">
                      <img class="member-pic" src="{{ asset('assets/theme/flondr/images/health-cate.jpg')}}" title="name" />
                      <h5><a href="#">Health & medical</a></h5>
                      <span>Hospital, clinic, etc</span>
                      <label class="team-member-caption text-center">
                        <p>sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem.</p>
                        <ul>

                          <div class="clearfix"> </div>
                        </ul>
                      </label>
                    </div>
                  </div>

                  <div class="col-md-3 team-member">
                    <div class="team-member-info">
                      <img class="member-pic" src="{{ asset('assets/theme/flondr/images/hotel-cate.jpg')}}" title="name" />
                      <h5><a href="#">Hotel & Travel</a></h5>
                      <span>Villa, Apartement, Tour, etc</span>
                      <label class="team-member-caption text-center">
                        <p>sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem.</p>
                        <ul>

                          <div class="clearfix"> </div>
                        </ul>
                      </label>
                    </div>
                  </div> 
                  <div class="clearfix"> </div>
                </div>
                <div class="clearfix"> </div>
              </div>
            </div>