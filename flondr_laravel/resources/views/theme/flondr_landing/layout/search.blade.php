
<!-- Divider: search artikel -->
<section class="bg-theme-color-2">
  <div class="container pt-0 pb-0">
    <div class="row">
      <div class="call-to-action pt-30 pb-20">
        <div class="col-md-6">
          <h3 class="mt-5 mb-5 text-white vertical-align-middle"><i class="fa fa-search mr-10 font-48 vertical-align-middle"></i> Pencarian Artikel / Berita</h3>
        </div>
        <div class="col-md-6">
          <!-- Mailchimp Subscription Form Starts Here -->
          {!! Form::open(array('url' => '/cari','method' => 'post'), ['class' => 'newsletter-form mt-10']) !!}
            <div class="input-group">
              <?php
              $value = null;
              if (isset($data['keyword']))
                $value = $data['keyword'];
              ?>
              {!! Form::text('keyword',$value, ['class' => 'form-control', 'placeholder' => 'Cari Berita/Artikel...', 'class' => 'form-control input-lg font-16', 'data-height' => '45px']) !!}
              <span class="input-group-btn">
                {!! Form::submit('Temukan',['class' => 'btn bg-theme-colored text-white btn-xs m-0 font-14', 'data-height' => '45px']) !!}
              </span>
            </div>
          {!! Form::close()!!}
          <br>
        </div>
      </div>
    </div>
  </div>
</section>
