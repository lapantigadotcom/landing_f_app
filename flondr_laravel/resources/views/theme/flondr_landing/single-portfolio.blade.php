@extends('theme.korlantas.layout.index')

@section('content')
 
    <div class="row" style="min-height:70%;">
      <div class="col-md-12">
 
    <div class="main-banner six" style="background:transparent url( {{ asset('theme/enagic/images/header-media2.jpg') }} ) repeat scroll center top">
      <div class="container">
                                <h2 style="visibility: visible; animation-name: fadeInLeft;" class="wow fadeInLeft animated">
<span>{!! ucwords($data['content']->title) !!}</span></h2>
      </div>
    </div>
    <div class="breadcrumb">
      <div class="container">
        <ul class="list-unstyled list-inline">
          <li><a href="{!! URL::to('/') !!}">Home</a></li>
          <li><a href="javascript:void(0);">{!! $data['content']->title !!}</a></li>
        </ul>
      </div>
    </div>

        <div class="col-md-offset-1 col-md-10">
         
         
 <div style="visibility: visible; animation-name: fadeInRight;" class="title-line wow fadeInRight animated">
                  </div>
          <p class="text-center">
            {!! $data['content']->description !!}
          </p>
          <br><br>
          <div class="row">
            <div class="col-md-12 gallery" style="float:left;">
            <?php 
            $i = 1;
            ?>
             @foreach($data['portfolio'] as $row)
             <?php
             
             if(($i-1)%4 == 0)
              echo "<div class='row'>";
             ?>
             <div class="col-md-3">
              @if(file_exists('./upload/'.$row->file))
              <a href="@if($row->type == '1' ) {{ asset('upload/media/'.$row->media->file) }} @else{!! url( trim($row->link) ) !!}@endif" rel="prettyPhoto[gallery1]" title="{{ $row->caption}}">
                <img src="{{ asset('upload/media/'.$row->media->file) }}" class="img-responsive" alt="{{ $row->caption}}">
              </a>
              @else
              <a href="@if($row->type == '1' ) {{ asset('upload/media/'.$row->media->file) }} @else{!! url(trim($row->link)) !!}@endif" rel="prettyPhoto[gallery1]" title="{{ $row->caption}}">
                <img src="{!! asset('theme/korlantas/img/user.png') !!}" class="img-responsive" alt="{{ $row->caption}}">
                @endif
                <center><h6>{{ $row->caption}}</h6>
                <p>{!! $row->description !!}</p></center>
                 <p>{!! $row->description !!}</p><br>
              </div>
              <?php 
              if($i%4 == 0 || $i == $data['portfolio']->count())
              {
                echo "</div>";
              }
                $i++;
               ?>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 text-center" style="margin-left:auto; margin-right:auto;">
        {!! $data['portfolio']->render() !!}
      </div>
    </div>
    
 @include('theme.korlantas.layout.search')


        @endsection

        @section('custom-head')
         
        @endsection

        @section('custom-footer')
        
        @endsection