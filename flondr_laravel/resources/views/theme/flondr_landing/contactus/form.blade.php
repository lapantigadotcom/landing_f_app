    <form>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                   {!! Form::label('name','Nama * : ') !!}
        {!! Form::text('name',null, ['class' => 'form-control']) !!}
         </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                   {!! Form::label('email','Email * : ') !!}
        {!! Form::text('email',null, ['class' => 'form-control']) !!}
                  </div>
                </div>
              </div>                
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    {!! Form::label('title','Judul * : ') !!}
      {!! Form::text('title',null, ['class' => 'form-control']) !!}
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                   {!! Form::label('nohp','No Hp : ') !!}
        {!! Form::text('nohp',null, ['class' => 'form-control']) !!}
                  </div>
                </div>
              </div>

              <div class="form-group">
               {!! Form::label('message','Pesan * : ') !!}
      {!! Form::textarea('message',null, ['class' => 'form-control','id' => 'textEditor']) !!}
              </div>
              <div class="form-group">
              {!! Form::submit($buttonSubmit,['class' => 'btn btn-flat btn-theme-colored text-uppercase mt-10 mb-sm-30 border-left-theme-color-2-4px']) !!}
              </div>      
            </form>