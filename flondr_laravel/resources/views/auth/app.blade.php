<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>LOGIN BACKEND</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link rel="stylesheet" type="text/css" href="{!! asset('css/bootstrap.min.css') !!}">
        <meta name="author" content="lapantiga.com" />
        <link rel="stylesheet" type="text/css" href="{!! asset('css/font-awesome.min.css') !!}">
         <link rel="stylesheet" type="text/css" href="{!! asset('css/adminlte/AdminLTE.css') !!}">
         <link rel="stylesheet" type="text/css" href="{!! asset('plugins/iCheck/square/blue.css') !!}">
        
    </head>
    <body class="login-page" style="background:#16174F">

        @yield('content')


        <!-- jQuery 2.0.2 -->
        <script src="{!! asset('plugins/jQuery/jQuery-2.1.4.min.js') !!}"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="{!! asset('js/bootstrap.min.js') !!}"></script>
        <script src="{!! asset('plugins/iCheck/icheck.min.js') !!}"></script>
        <!-- Bootstrap -->
      
        <script>
          $(function () {
            $('input').iCheck({
              checkboxClass: 'icheckbox_square-blue',
              radioClass: 'iradio_square-blue',
              increaseArea: '20%' // optional
            });
          });
        </script>

    </body>
</html>