@extends('layouts.app')

@section('content')
<div class="container" >
    <div class="row">
        <div class="col-md-12"> 
            <img src="{{asset('assets/theme/flondr/images/logo_flondr.png')}}" style="max-width:120px;padding-top: 60px" class="img-responsive center-block"> 
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login | Flondr Backend <span style="font-size:12;color:red">Pastikan "cek" recaptcha</span></div>
                <div class="panel-body">
                    <form class="form-horizontal gcaptcha" role="form" method="POST" action="{{ route('login') }}" >
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Username</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-4">
                        <div class="form-group">
                          <div class="g-recaptcha" id="recaptcha" data-sitekey="6Lc7uCoUAAAAAOt9FiFLjdOcHbyg7n6lnI7Sp5lJ"></div>
                      </div> </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.reset') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center" style="color:#fff"> Copyright © 2017 PT. Gatam Perkasa Media. All rights reserved. </div>
    </div>
</div>
@endsection

<script type="text/javascript">
var gcaptchaVerify = 0; 
        function reCaptchaVerify(response){gcaptchaVerify = 1;} 
        var onloadCallback = function() { 
                // grecaptcha.render('recaptcha', { 'sitekey' : '6Lc7uCoUAAAAAOt9FiFLjdOcHbyg7n6lnI7Sp5lJ', 'callback': reCaptchaVerify }); 
                grecaptcha.render('recaptcha', { 'sitekey' : '6LdNugoUAAAAAKer7jMhmFx8PCzQjBGESYtRi8Kk', 'callback': reCaptchaVerify }); 
                
        }; 
        $(".gcaptcha").on('submit', function(e){ 
                if (gcaptchaVerify==0)e.preventDefault();
        })
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

