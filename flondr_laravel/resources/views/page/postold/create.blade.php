@extends('app')
@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						TAMBAH POST BARU
					</h3>                                    
				</div>
				<div class="box-body">
					{!! Form::open(array('url' => 'vrs-admin/post','method' => 'post')) !!}
					<div class="col-md-9">
						@include('page.errors.list')
						@include('page.post.form',['submitText' => 'Tambahkan'])
					</div>
					<div class="col-md-3">
						<?php 
						$arrPortfolio = array();
						$arrPortfolio[0] = 'Tidak Ada';
						foreach($data['portfolio'] as $row)
						{
							$arrPortfolio[$row->id] = $row->title;
						}
						?>
						@include('page.post.right-sidebar',['arrPortfolio' => $arrPortfolio])
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.partials.media')

@endsection

@section('custom-head')

@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/jqueryValidation/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('plugins/tinymce/js/tinymce/tinymce.min.js')}}"></script>
@include('page.post.script')
@stop
