		<div class="modal fade" id="mediaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Set Featured Image</h4>
					</div>
					<div class="modal-body">
						<div role="tabpanel">
							<ul class="nav nav-tabs" id="mediaTab"  role="tablist">
								<li role="presentation"><a href="#uploadMedia"  aria-controls="uploadMedia" role="tab" data-toggle="tab">Upload</a></li>
								<li role="presentation" class="active"><a href="#listMediaContainer"  aria-controls="listMediaContainer" role="tab" data-toggle="tab">Media</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div id="uploadMedia" role="tabpanel" class="tab-pane">
								{!! Form::open(array('url' => 'vrs-admin/media/storeAjax','method' => 'post', 'id' => 'mediaUploadForm')) !!}
								<div class="form-group">
									{!! Form::label('file','File : ') !!}
									{!! Form::file('file', ['class' => 'form-control','id' => 'mediaBrowse']) !!}
								</div>
								{!! Form::close() !!}
								<div id='preloader' style='display:none'>
									<span class="glyphicon glyphicon-refresh preloader"></span>
								</div>
							</div>
							<div id="listMediaContainer" role="tabpanel" class="tab-pane active">
								<p>
									<div id="listMedia" class="row">
										
									</div>
								</p>
							</div>
						</div>
						
						
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
						<button type="button" class="btn btn-primary" onclick="setFeatured()">Set Gambar Fitur</button>
					</div>
				</div>
			</div>
		</div>