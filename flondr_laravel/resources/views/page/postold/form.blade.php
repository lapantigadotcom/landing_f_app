		<div class="form-group">
			{!! Form::label('title','Judul : ') !!}
			{!! Form::text('title',null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('title_eng','Judul (eng): ') !!}
			{!! Form::text('title_eng',null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('description','Deskripsi : ') !!}
			{!! Form::textarea('description',null, ['class' => 'form-control textEditor','id' => 'textEditor']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('eng_description','Description (English) : ') !!}
			{!! Form::textarea('eng_description',null, ['class' => 'form-control textEditor','id' => 'textEditor']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('show_comment','Komentar : ') !!}
			<label>
      			{!! Form::checkbox('show_comment','1',null, ['class' => '','id' => '']) !!} 
    		</label>
			
		</div>
		<div class="form-group">
			{!! Form::submit($submitText,['class' => 'btn btn-info']) !!}
		</div>