@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						MEMBER {!! HTML::link(route('vrs-admin.club.create'),'Tambah Baru',['class' => 'btn btn-default btn-add-new']) !!}
					</h3>                                    
				</div>
				<div class="box-body table-responsive">
					@include('page.partials.notification')
					<div class="row">
						<div class="col-md-10">
							<table class="table" id="dataTables">
								<thead>
									<tr>
										<th>&nbsp;</th>
										<th>Nama</th>
										<th>Tipe Club</th>
										<th>Kota</th>
										<th>Reward</th>
										<th>Photo</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data['content'] as $row)
									<tr>
										<td>&nbsp;</td>
										<td>
											{{ $row->name }}
											<div class="action-post-hover">
												{!! HTML::link(route('vrs-admin.club.edit',[ $row->id ]),'edit') !!}
												<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('vrs-admin.club.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
											</div>
										</td>
										<td><b>{{ ucfirst($row->typeclub->name) }}</b> </td>
										<td>{{ $row->city }}</td>
										<td>
										{{ $row->reward }}</td>
										<td>
											{{ $row->address }}
										</td>
										<td>
											@if(isset($row->photo))
							{!! HTML::image(asset('upload/member/'.$row->photo),'',['class' => 'col-md-5 img-thumbnail']) !!}
						@endif
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/dataTables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/dataTables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
	$('.radio-featured').click(function() {
		$('.radio-featured').prop('checked',false);
		$(this).prop('checked',true);			
		var id = $(this).val();
		var url = "{{ URL::to('vrs-admin/club/enabled') }}/" + id +"";
		$.ajax(url)
		.done(function() {
			
		})
		.fail(function() {
			alert( "error" );
		});
	})
</script>
@stop