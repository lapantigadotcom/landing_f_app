@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						EDIT EVENT
					</h3>                                    
				</div>
				<div class="box-body">
					{!! Form::model($data['content'],array('action' => ['EventController@update',$data['content']->id],'method' => 'PATCH')) !!}
					<div class="col-md-9">
						@include('page.errors.list')
						@include('page.event.form',['submitText' => 'Perbarui'])
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>

@endsection

@section('custom-head')

@stop
@section('custom-footer')

<script src="('plugins/jqueryValidation/jquery.validate.min.js')"></script>
<script src="('plugins/tinymce/js/tinymce/tinymce.min.js')"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>
	$(function() {
		$( "#datepicker" ).datepicker({
			dateFormat: "dd-mm-yy"
		});
	});
</script>
@stop
