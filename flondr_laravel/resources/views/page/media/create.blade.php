@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Tambah Media
					</h3>                                    
				</div>
				<div class="box-body">
					{!! Form::open(array('route' => 'media.store', 'method' => 'post','files' => true)) !!}
					@include('page.errors.list')
					@include('page.media.form',['submitText' => 'Tambah'])
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('custom-head')

@stop
@section('custom-footer')
<script src="{{ asset('plugins/bootstrapFileStyle/bootstrap-filestyle.min.js') }}"></script>
<script type="text/javascript">
	$('input[type=file]').filestyle();
</script>
@stop