		<div class="form-group">
			{!! Form::label('file','File : ') !!}
			<div class="col-md-12" id="featuredImage">
			@if(isset($data['content']->file))
				<img src="{{asset('upload/media/'.$data['content']->file)}}" class="col-md-5 img-thumbnail">
			@endif
			</div>
			{!! Form::file('file', ['class' => 'form-control']) !!}
			
		</div>
		<div class="form-group">
			{!! Form::label('caption','Caption : * ') !!}
			{!! Form::text('caption',null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('alternate_text','Alternate Text : *') !!}
			{!! Form::text('alternate_text',null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('description','Deskripsi : *') !!}
			{!! Form::textarea('description',null, ['class' => 'form-control','id' => 'textEditor']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit($submitText,['class' => 'btn btn-info']) !!}
		</div>