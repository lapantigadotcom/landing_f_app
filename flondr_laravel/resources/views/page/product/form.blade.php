		<div class="form-group">
			{!! Form::label('name','Nama : ') !!}
			{!! Form::text('name',null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('description','Deskripsi : ') !!}
			{!! Form::textarea('description',null, ['class' => 'form-control','id' => 'textEditor']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('ms_categories_id','Kategori* : ') !!}
			{!! Form::select('ms_categories_id',$arrCategory,null, ['class' => 'form-control']) !!}			
		</div>
		<div class="form-group" id="mediaformcontainer">
			{!! Form::label('ms_media_id','Gambar : ') !!}
			<div class="col-md-12" id="featuredImage">
			</div>
			<a href="javascript:void(0)" class="btn btn-default" onclick="setFeaturedImage()">Pilih Media</a>
			{!! Form::hidden('ms_media_id',null, ['class' => 'form-control','id' => 'ms_media_id']) !!}
		</div>
		<div class="form-group">
			{!! Form::submit($submitText,['class' => 'btn btn-info']) !!}
		</div>