@extends('app')
@section('content')
<section class="content-header">
  <h1>
    Dashboard
    <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Dashboard</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-xs-4">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Browser Visitor</h3>                                    
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body table-responsive">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>
                  Link Halaman
                </th>
                <th>
                  Dilihat
                </th>
              </tr>
            </thead>
            <tbody>
              @foreach($data['stat_pages'] as $row)
              <tr>
                <th>  {{ $row["sessions"] }}</th>
                <th>{{ $row["browser"] }}</th>
              </tr>
              @endforeach
            </tbody>
          </table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
    <div class="col-xs-8">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Statistik Page View dan Visitors</h3>                                    
          <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>

        <div class="box-body table-responsive">
          <div class="chart">
            <canvas id="lineChart" height="250"></canvas>
          </div>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div>
  </div>
 <div class="row">

  <div class="col-md-12">
   <!-- BEGIN SAMPLE TABLE PORTLET-->
   <div class="portlet box yellow">
    <div class="portlet-title">
     <div class="caption"><i class="icon-pencil"></i>Artikel Terbaru</div>
     <div class="tools">
      <a href="javascript:;" class="collapse"></a>
      <a href="#portlet-config" data-toggle="modal" class="config"></a>
      <a href="javascript:;" class="reload"></a>
      <a href="javascript:;" class="remove"></a>
    </div>
  </div>
  <div class="portlet-body">
   <div class="table-responsive">
    <table class="table table-hover">
      <thead>
       <tr>
         <th>ID</th>
         <th>Judul</th>
         <th>Penulis</th>
         <th>Kategori</th>
         <th>Tgl Updated</th>

       </tr>
     </thead>
     <tbody>
      @foreach($data['artikellist'] as $row)
      <?php 
      if($row->user == null)
        continue;
      ?>
      <tr>
        <td>{{ $row->id }}</td>
        <td>
          {{ $row->title }}
          <div class="action-post-hover">
          </div>
        </td>
        <td><span class="label label-sm label-warning">{{ $row->user->name }}</span></td>
        <td>
          <?php $category = array(); ?>
          @foreach($row->category as $v)
          <?php array_push($category, $v->title) ?>
          @endforeach
          {!! implode(', ', $category) !!}
        </td>


        <td>
          {!! date('d/m/Y | H:i:s',strtotime($row->updated_at)) !!}
        </td>

      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
</div>
<div class="col-md-12">
 <!-- BEGIN SAMPLE TABLE PORTLET-->
 <div class="portlet box yellow">
  <div class="portlet-title">
   <div class="caption"> INBOX CONTACT US</div>
   <div class="tools">
    <a href="javascript:;" class="collapse"></a>
    <a href="#portlet-config" data-toggle="modal" class="config"></a>
    <a href="javascript:;" class="reload"></a>
    <a href="javascript:;" class="remove"></a>
  </div>
</div>
<div class="portlet-body">
 <div class="table-responsive">
  <table class="table table-hover">
    <thead>
     <tr>
      <th>Email Pengirim</th>
      <th>Nama</th>
      <th>Isi</th>
      <th>Tanggal Dikirim</th>
  

    </tr>
  </thead>
  <tbody>
    @foreach($data['inboxkontak'] as $m)

    <tr>
      <td>  {{ $m->email }}</td>
      <td>
        {{ $m->nama }}

      </td>
      <td>
       {{$m->isi}}
     </td>
     <td>
      {{ date('d/m/Y',strtotime($row->created_at)) }}
    </td>
   

  </tr>
  @endforeach
</tbody>
</table>
</div>
</div>
</div>
<!-- END SAMPLE TABLE PORTLET-->
</div>

</div>

<div class="row">
  <div class="col-xs-12">
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-aqua">
          <span class="info-box-icon"><i class="icon-archive"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Artikel</span>
            <span class="info-box-number">{!! $data['artikeltotal'] !!} </span>

            <div class="progress">
              <div class="progress-bar" style="width: {!! $data['artikeltotal'] !!}%"></div>
            </div>
            <span class="progress-description">
             <a style="color:#fff" href="{{ url('vrs-admin/post') }}"><i class="icon-arrow-right">&nbsp;</i> lihat artikel </a>
           </span>
         </div>
         <!-- /.info-box-content -->
       </div>
       <!-- /.info-box -->
     </div>
     <!-- /.col -->
     <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="info-box bg-green">
        <span class="info-box-icon"><i class="icon-info"></i></span>

        <div class="info-box-content">
          <span class="info-box-text">Navigasi</span>
          <span class="info-box-number">{!! $data['menutotal'] !!}</span>

          <div class="progress">
            <div class="progress-bar" style="width: {!! $data['menutotal'] !!}%"></div>
          </div>
          <span class="progress-description">
           <a style="color:#fff" href="{{ url('vrs-admin/menu') }}"><i class="icon-arrow-right">&nbsp;</i> lihat menu Utama </a>
         </span>
       </div>
       <!-- /.info-box-content -->
     </div>
     <!-- /.info-box -->
   </div>
   <!-- /.col -->
   <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box bg-yellow">
      <span class="info-box-icon"><i class="icon-camera"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Images</span>
        <span class="info-box-number">{!! $data['gambartotal'] !!}</span>

        <div class="progress">
          <div class="progress-bar" style="width: {!! $data['gambartotal'] !!}%"></div>
        </div>
        <span class="progress-description">
         <a style="color:#fff" href="{{ url('vrs-admin/media') }}"><i class="icon-arrow-right">&nbsp;</i>library images </a>
       </span>
     </div>
     <!-- /.info-box-content -->
   </div>
   <!-- /.info-box -->
 </div>
 <!-- /.col -->
 <div class="col-md-3 col-sm-6 col-xs-12">
  <div class="info-box bg-red">
    <span class="info-box-icon"><i class="icon-shopping-cart"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Produk</span>
      <span class="info-box-number">{!! $data['gambartotal'] !!}</span>

      <div class="progress">
        <div class="progress-bar" style="width: {!! $data['gambartotal'] !!}%"></div>
      </div>
      <span class="progress-description">
       <a style="color:#fff" href="{{ url('vrs-admin/leader') }}"><i class="icon-arrow-right">&nbsp;</i>lihat produk </a>
     </span>
   </div>
   <!-- /.info-box-content -->
 </div>
 <!-- /.info-box -->
</div>
<!-- /.col -->
</div>                       
</div>
</div>

</section><!-- /.content -->
@endsection

@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
<script type="text/javascript">
 var areaChartOptions = {
          //Boolean - If we should show the scale at all
          showScale: true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines: false,
          //String - Colour of the grid lines
          scaleGridLineColor: "rgba(0,0,0,.05)",
          //Number - Width of the grid lines
          scaleGridLineWidth: 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines: true,
          //Boolean - Whether the line is curved between points
          bezierCurve: true,
          //Number - Tension of the bezier curve between points
          bezierCurveTension: 0.3,
          //Boolean - Whether to show a dot for each point
          pointDot: false,
          //Number - Radius of each point dot in pixels
          pointDotRadius: 4,
          //Number - Pixel width of point dot stroke
          pointDotStrokeWidth: 1,
          //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
          pointHitDetectionRadius: 20,
          //Boolean - Whether to show a stroke for datasets
          datasetStroke: true,
          //Number - Pixel width of dataset stroke
          datasetStrokeWidth: 2,
          //Boolean - Whether to fill the dataset with a color
          datasetFill: true,
          //String - A legend template
          legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
          //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
          maintainAspectRatio: false,
          //Boolean - whether to make the chart responsive to window resizing
          responsive: true
        };
        var areaChartData = {
          labels: [
          @foreach($data['stat_visitors'] as $row)
          "{{ $row["date"]->format('j M') }}",

          @endforeach
          ],
          datasets: [
          {
            label: "Visitors",
            fillColor: "rgba(210, 214, 222, 1)",
            strokeColor: "rgba(210, 214, 222, 1)",
            pointColor: "rgba(210, 214, 222, 1)",
            pointStrokeColor: "#c1c7d1",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: [
            @foreach($data['stat_visitors'] as $row)
            {{ $row["visitors"]."," }}
            @endforeach
            ]
          },
          {
            label: "PageViews",
            fillColor: "rgba(60,141,188,0.9)",
            strokeColor: "rgba(60,141,188,0.8)",
            pointColor: "#3b8bba",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: [
            @foreach($data['stat_visitors'] as $row)
            {{ $row["pageViews"]."," }}
            @endforeach
            ]
          }
          ]
        };
        var lineChartCanvas = $("#lineChart").get(0).getContext("2d");
        var lineChart = new Chart(lineChartCanvas);
        var lineChartOptions = areaChartOptions;
        lineChartOptions.datasetFill = false;
        lineChart.Line(areaChartData, lineChartOptions);
      </script>   
    @endsection
