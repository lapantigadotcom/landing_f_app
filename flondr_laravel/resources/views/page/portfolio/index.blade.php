@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						MEDIA LIST
						<a href="{{route('portfolio.create')}}" class="btn btn-default btn-add-new">Tambah Baru</a>
					</h3>                                    
				</div>
				<div class="box-body  table-responsive">
					@include('page.partials.notification')
					<table class="table" id="dataTables">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Judul</th>
								<th>Deskripsi</th>
								<th>Parent</th>
								<!-- <th>Featured</th> -->
								<!-- <th>Enabled</th> -->
								<th>Total</th>
								<th>Tanggal</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data['content'] as $row)
							<tr>
								<td>&nbsp;</td>
								<td>
									{{ $row->title }}
									<div class="action-post-hover">
										<a href="{{route('portfolio.show',[ $row->id ])}}">detail</a>
										<a href="{{route('portfolio.edit',[ $row->id ])}}">edit</a>
										<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('portfolio.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
									</div>
								</td>
								<td>{{ substr($row->description,0,100) }}</td>
								<td>
									{{ $row->parent_id=='0'?'Tidak Ada':substr($row->parent->title,0,20) }}
								</td>
								<!-- <td>
									<div class="Switch Round {!! $row->featured=='1'?'On':'Off' !!}" data-id="{!! $row->id !!}">
										<div class="Toggle"></div>
									</div>
								</td> -->
								<!-- <td>@if($row->enabled=='1')
									<a href="{{route('portfolio.enabled',[ $row->id, '0'])}}">edit</a>
									@else
									<a href="{{route('portfolio.enabled',[ $row->id, '1'])}}">edit</a>
									@endif
								</td> -->
								<td>{{ $row->detailPortfolio()->count() }}</td>
								<td>
									{!! date('d/m/Y',strtotime($row->updated_at)) !!}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
	$('#dataTables').on('click', '.Switch', function()  {
		var id_post = $(this).data("id");
		setFeatured(id_post);
		$(this).toggleClass('On').toggleClass('Off');
	});
	function setFeatured(id)
	{
		var url = "{{ URL::to('vrs-admin/portfolio/featured') }}/" + id +"";
		$.ajax(url)
		.done(function() {
			
		})
		.fail(function() {
			alert( "error" );
		});
	}
</script>
@stop