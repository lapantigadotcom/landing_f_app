@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						PORTFOLIO <strong><em> {{ $data['content']->portfolio->title }}</em></strong>
					</h3>                                    
				</div>
				<div class="box-body">
			<div class="row">
				<div class="col-md-10">
				<h5>Edit Detail Portfolio</h5>
				@include('page.errors.list')
				{!! Form::model($data['content'],['action'=>['DetailPortfolioController@update',$data['content']->id], 'method' => 'PATCH']) !!}
					@include('page.portfolio.form-detail',['buttonSubmit' => 'Perbarui','ms_portfolio_id' => $data['content']->ms_portfolio_id ])
				{!! Form::close() !!}
				</div>
			</div>
		</div>
		</div>
		</div>
		</div>
		</section>
		@include('page.partials.media')
@endsection
@section('custom-footer')
	@include('page.scripts.media',['featuredImage' => $data['content']->media->file ])
	<script type="text/javascript">
					$(document).ready(function() {
						// var reference = (function tipeportfolio(){
						// 	var type = $('#type').val();
						// 	switch(type){
						// 		case '1':
						// 			$('#mediaformcontainer').show();
						// 		break;
						// 		case '2':
						// 			$('#mediaformcontainer').hide();
						// 		break;
						// 	}
						//     return tipeportfolio;
						// }());
						// $('#type').change(function(){
						// 	reference();
						// 	console.log("ganti");
						// });
					});
					</script>
@stop