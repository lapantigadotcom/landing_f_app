@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Tipe Club
					</h3>                                    
				</div>
				<div class="box-body">

					@include('page.partials.notification')
					<div class="row">
						<div class="col-md-12">
							<p>
								Untuk menampilkan daftar member clup pada halaman depan. <br>
								Gunakan url <b>{{ url('/member')}}/[kode klub]</b><br> misal: {{ url('/member')}}/core-leader , jika kode tipe club = core-leader
							</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							<h5>Tambah Tipe Club Baru</h5>
							{!! Form::open(['route'=>'vrs-admin.typeclub.store', 'method' => 'store']) !!}
							@include('page.typeclub.form',['buttonSubmit' => 'Tambah'])
							{!! Form::close() !!}
						</div>
						<div class="col-md-9">
							<div class="row">

							</div>
							<table class="table" id="dataTables">
								<thead>
									<tr>
										<th>&nbsp;</th>
										<th>Nama</th>
										<th>Kode</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									@foreach($data['content'] as $row)
									<tr>
										<td>&nbsp;</td>
										<td>
											{{ $row->name }}
											<div class="action-post-hover">
												{!! HTML::link(route('vrs-admin.typeclub.edit',[ $row->id ]),'edit') !!}
												<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('vrs-admin.typeclub.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
											</div>
										</td>
										<td>{{ $row->code }}</td>
										<td>
											{{ $row->club->count() }}
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/dataTables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/chartjs/Chart.min.js') }}"></script>
<script type="text/javascript">
	
</script>
@stop