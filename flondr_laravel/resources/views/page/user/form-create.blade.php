					<div class="form-group">
						<div class="form-group col-md-6">
							{!! Form::label('name','Nama Lengkap* : ') !!}
							{!! Form::text('name',null, ['class' => 'form-control']) !!}
						</div>
						<div class="form-group col-md-6">
							{!! Form::label('panggilan','Panggilan* : ') !!}
							{!! Form::text('panggilan',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							{!! Form::label('custom','Custom Link (hanya angka/huruf tanpa spasi, contoh : gemarsehati) : ') !!}
							{!! Form::text('custom',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('notelp','No. Telpon : ') !!}
							{!! Form::text('notelp',null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('nohp','No. HP* : ') !!}
							{!! Form::text('nohp',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('ms_gender_id','Jenis Kelamin* : ') !!}
							{!! Form::select('ms_gender_id',$arrGender,null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('address','Alamat Lengkap : ') !!}
							{!! Form::text('address',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('ms_city_id','Kota* : ') !!}
							{!! Form::select('ms_city_id',$arrCity,null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-3">
							{!! Form::label('kodepos','Kode Pos : ') !!}
							{!! Form::text('kodepos',null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-3">
							{!! Form::label('pin_bb','Pin BB : ') !!}
							{!! Form::text('pin_bb',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							{!! Form::label('coord','Koordinat alamat : ') !!}
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								{!! Form::label('latitude','Latitude : ') !!}
								{!! Form::text('latitude',null, ['class' => 'form-control']) !!}
							</div>
							<div class="col-md-6">
								{!! Form::label('longitude','Longitude : ') !!}
								{!! Form::text('longitude',null, ['class' => 'form-control']) !!}
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('email','Email* : ') !!}
							{!! Form::text('email',null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('password','Password* (min 6 karakter): ') !!}
							{!! Form::password('password', array('class' => 'form-control')) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('facebook','Facebook : ') !!}
							{!! Form::text('facebook',null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('twitter','Twitter : ') !!}
							{!! Form::text('twitter',null, ['class' => 'form-control']) !!}
						</div>
						<!-- <div class="col-md-6">
							{!! Form::label('password_conf','Password Confirmation* : ') !!}
							{!! Form::password('password_conf', array('class' => 'form-control')) !!}
						</div> -->
					</div>					
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('sn_mesin','SN Mesin : ') !!}
							{!! Form::text('sn_mesin',null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('jenis_mesin','Jenis Mesin : ') !!}
							{!! Form::text('jenis_mesin',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group" id="mediaformcontainer">
						<div class="col-md-12">
							{!! Form::label('ms_media_id','Foto : ') !!}
							<div class="col-md-12" id="featuredImage">
							</div>
							<a href="javascript:void(0)" class="btn btn-default" onclick="setFeaturedImage()">Pilih Media</a>
							{!! Form::hidden('ms_media_id',null, ['class' => 'form-control','id' => 'ms_media_id']) !!}
						</div>
					</div>
					<div class="form-group">
						<!-- <div class="col-md-6">
							{!! Form::label('media','Upload Foto : ') !!}
						    <input type="file" id="exampleInputFile">
						    <p class="help-block">support file format: jpeg,bitmap,gif,png</p>
					    </div>-->
    					<!-- <div class="col-md-6">
							<script src='https://www.google.com/recaptcha/api.js'></script>
							<div class="g-recaptcha" data-sitekey="6LcpGhYTAAAAANxHp9_mlDtRcm1kCA5t7gxJ28Qw"></div>
						</div> -->
					</div>
					<div class="form-group col-md-12">
						{!! Form::submit($submitText,['class' => 'btn btn-info']) !!}
					</div>