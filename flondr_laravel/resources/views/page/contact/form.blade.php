					@include('page.errors.list')
					<div class="form-group col-md-12">
						{!! Form::label('name','Nama : ') !!}
						{!! Form::text('name',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group col-md-12">
						{!! Form::label('email','Email : ') !!}
						{!! Form::text('email',null, ['class' => 'form-control']) !!}
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('telephone','Telp : ') !!}
							{!! Form::text('telephone',null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('facebook','Facebook : ') !!}
							{!! Form::text('facebook',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							{!! Form::label('twitter','Twitter : ') !!}
							{!! Form::text('twitter',null, ['class' => 'form-control']) !!}
						</div>
						<div class="col-md-6">
							{!! Form::label('instagram','Instagram : ') !!}
							{!! Form::text('instagram',null, ['class' => 'form-control']) !!}
						</div>
					</div>
					<div class="form-group col-md-12">
						{!! Form::label('address','Alamat : ') !!}
						{!! Form::textarea('address',null, ['class' => 'form-control','rows'=>'3']) !!}
					</div>
					<div class="form-group col-md-12">
						{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
					</div>