@extends('app')
@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						MENU
						<a href="{{route('vrs-admin.menu.create')}}" class="btn btn-default btn-add-new">Tambah baru</a>
					</h3>                                    
				</div>
				<div class="box-body table-responsive">
					@include('page.partials.notification')
					<table class="table" id="dataTables">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Judul</th>
								<th>Kode</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data['content'] as $row)
							<tr>
								<td>&nbsp;</td>
								<td>
									<a href="{{route('vrs-admin.menu.show', [$row->id])}}">{{$row->title}}</a>
									<div class="action-post-hover">
										<a href="{{route('vrs-admin.menu.show',[$row->id])}}">details</a>
										<a href="{{route('vrs-admin.menu.edit',[ $row->id ])}}">edit</a>
										<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('vrs-admin.menu.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
									</div>
								</td>
								<td>
									{{ $row->code }}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
</script>
@stop