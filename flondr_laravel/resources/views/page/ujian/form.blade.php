<div class="form-group">
	{!! Form::label('ms_media_id','Gambar : ') !!}
	<a href="javascript:void(0)" class="btn btn-default" onclick="setFeaturedImage()">Pilih Media</a>
	<div class="col-md-12" id="featuredImage">
	</div>
	{!! Form::hidden('ms_media_id',null, ['class' => 'form-control','id' => 'ms_media_id']) !!}
</div>
<div class="form-group">
	{!! Form::label('title','Nama : ') !!}
	{!! Form::text('title',null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
	{!! Form::label('description','Deskripsi : ') !!}
	{!! Form::textarea('description',null, ['class' => 'form-control','rows'=>'3']) !!}
</div>
<div class="form-group">
	{!! Form::submit($buttonSubmit,['class' => 'btn btn-info']) !!}
</div>
