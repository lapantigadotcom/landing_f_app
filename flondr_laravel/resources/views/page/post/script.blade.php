<script>
	$('#newCategoryForm').hide();
	$('#newTagForm').hide();
	tinymce.init(
	{	 
		plugins: [
     "advlist autolink link image lists charmap print preview hr anchor pagebreak code",
     "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
     "table contextmenu directionality emoticons paste textcolor"
		],
		selector:'.textEditor',
		toolbar: "styleselect formatselect  fontselect fontsizeselect  | ,bold italic underline | blockquote | media | image | alignleft aligncenter alignright alignjustify | outdent indent | bullist numlist | cut copy paste |  undo redo | removeformat subscript superscript",
		image_advtab: true ,
		image_list: "{{ URL::to('/imglist') }}",
		relative_urls: false,
		remove_script_host: false
	});
	function addCategory() {
		$('#newCategoryForm').toggle();
	}
	function postCategory(){
		var title = $('#newCategory').val();
		if(title == '')
			return false;
		$.get("{{ URL::to('vrs-admin/category/storeAjax') }}",
	        {
	          title: title,
	        },
	        function(data,status){
	            if(status=='success')
	            {
	            	var html = "<div class='form-group'><label><input type='checkbox' name='categories[]' checked value='" + data + "'> " + title +"</label></div>";
	            	$('#newCategory').val('');
	            	$('#listCategory').append(html);
	            	console.log(data);
	            }

	        }
        );
	}
	function addTag() {
		$('#newTagForm').toggle();
	}
	function postTag(){
		var title = $('#newTag').val();
		if(title == '')
			return false;
		$.get("{{ URL::to('vrs-admin/tag/storeAjax') }}",
	        {
	          title: title,
	        },
	        function(data,status){
	            if(status=='success')
	            {
	            	var html = "<div class='col-md-6'><label><input type='checkbox' name='tags[]' checked value='" + data + "'> " + title +"</label></div>";
	            	$('#newTag').val('');
	            	$('#listTag').append(html);
	            	console.log(data);
	            }

	        }
        );
	}

</script>
	@if(isset($data['content']) && $data['content']->ms_media_id!='0' )
		@include('page.scripts.media',['featuredImage' => $data['content']->mediaPost->file])
	@else
		@include('page.scripts.media')
	@endif