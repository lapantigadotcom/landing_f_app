			<div class="panel panel-default">
		  		<div class="panel-heading">Kategori</div>
				<div class="panel-body">
					<div id="listCategory">
					<?php $arrCategory = array(); ?>
					@if(isset($data['content']))
						
						@foreach($data['content']->category as $row)
						<?php array_push($arrCategory, $row->id); ?>
						@endforeach
						
					@endif
			    	@foreach($data['categories'] as $row)
			    		<div class="form-group">
			    			<label>
		    					<input type="checkbox" name="categories[]" {{ in_array($row->id, $arrCategory)?'checked':'' }} value="{!! $row->id !!}"> {!! $row->title !!}
		    				</label>
			    		</div>
			    	@endforeach
			    	</div>
			    	<!-- <a href="javascript:void(0)" onclick="addCategory()">+ Tambah Kategori</a>
			    	<div class="col-md-12" id="newCategoryForm">
			    		<div class="form-group">
			    			{!! Form::text('newCategory',null, ['class' => 'form-control','placeholder' => 'Ketikan kategori','id' => 'newCategory']) !!}
			    		</div>
			    		<div class="form-group">
			    			<a href="javascript:void(0)" class="btn btn-default" onclick="postCategory()">Tambah Kategori Baru</a>
			    		</div>
			    	</div> -->
			  	</div>
			</div>
			<div class="panel panel-default">
		  		<div class="panel-heading">Tag</div>
				<div class="panel-body">
					<div id="listTag" class="row">
					<?php $arrTag = array(); ?>
					@if(isset($data['content']))
						
						@foreach($data['content']->tag as $row)
						<?php array_push($arrTag, $row->id); ?>
						@endforeach
						
					@endif
			    	@foreach($data['tags'] as $row)
			    		<div class="col-md-6">
			    			<label>
		    					<input type="checkbox" name="tags[]" {{ in_array($row->id, $arrTag)?'checked':'' }} value="{!! $row->id !!}"> {!! $row->title !!}
		    				</label>
			    		</div>
			    	@endforeach
			    	</div>
			    	<!-- <a href="javascript:void(0)" onclick="addTag()">+ Tambah Tag</a>
			    	<div class="col-md-12" id="newTagForm">
			    		<div class="form-group">
			    			{!! Form::text('newTag',null, ['class' => 'form-control','placeholder' => 'Ketikan tag','id' => 'newTag']) !!}
			    		</div>
			    		<div class="form-group">
			    			<a href="javascript:void(0)" class="btn btn-default" onclick="postTag()">Tambah Tag Baru</a>
			    		</div>
			    	</div> -->
			  	</div>
			</div>
			<div class="panel panel-default">
		  		<div class="panel-heading">Meta Deskripsi</div>
				<div class="panel-body">
					<div class="form-group">
						{!! Form::textarea('meta_description',null,['class' => 'form-control','rows' => '3']) !!}
					</div>
			  	</div>
			</div>
			<div class="panel panel-default">
		  		<div class="panel-heading">Portfolio Relasi</div>
				<div class="panel-body">
					<div class="form-group">
						{!! Form::select('ms_portfolio_id',$arrPortfolio,null, ['class' => 'form-control']) !!}
					</div>
			  	</div>
			</div>
			<div class="panel panel-default">
		  		<div class="panel-heading">Gambar Fitur</div>
				<div class="panel-body">
					<div class="col-md-12" id="featuredImage">
					
					</div>
					{!! Form::hidden('ms_media_id',isset($data['content'])?$data['content']->ms_media_id:'',['id' => 'ms_media_id']) !!}
					<div class="col-md-12">
						<div class="col-md-6">
				    		<a href="javascript:void(0)" onclick="setFeaturedImage()">+ Set Gambar</a>
				    	</div>
				    	<div class="col-md-6">
				    		<a href="javascript:void(0)" id="mediaUnset" onclick="mediaUnset()" style="color:red;">- Hapus Gambar</a>
				    	</div>
			    	</div>

			  	</div>
			</div>