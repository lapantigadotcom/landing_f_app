@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					                                   
				</div>
				<div class="box-body table-responsive">
					@include('page.partials.notification')
					<table class="table" id="dataTables">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Judul</th>
								<th>Penulis</th>
								<th>Kategori</th>
								<th>Tag</th>
								<th>Featured</th>
								<th>Tanggal</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data['content'] as $row)
							<?php 
								if($row->user == null)
									continue;
							?>
							<tr>
								<td>&nbsp;</td>
								<td>
									{{ $row->title }}
									<div class="action-post-hover">
										<a href="{!! route('post.edit',[ $row->id ]) !!}" style="color:orange;">edit</a>
 										<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('post.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
									</div>
								</td>
								<td>{{ $row->user->name }}</td>
								<td>
									<?php $category = array(); ?>
									@foreach($row->category as $v)
									<?php array_push($category, $v->title) ?>
									@endforeach
									{{ implode(', ', $category) }}
								</td>
								<td>
									<?php $tag = array(); ?>
									@foreach($row->tag as $v)
									<?php array_push($tag, $v->title) ?>
									@endforeach
									{{ implode(', ', $tag) }}
								</td>
								<td>
									<div class="Switch Round {!! $row->featured=='1'?'On':'Off' !!}" data-id="{!! $row->id !!}">
										<div class="Toggle"></div>
									</div>
								</td>
								<td>
									{{ date('d/m/Y',strtotime($row->updated_at)) }}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
	$('#dataTables').on('click', '.Switch', function()  {
		var id_post = $(this).data("id");
		setFeatured(id_post);
		$(this).toggleClass('On').toggleClass('Off');
	});
	function setFeatured(id)
	{
		var url = "{{ URL::to('vrs-admin/post/featured') }}/" + id +"";
		$.ajax(url)
		.done(function() {
			
		})
		.fail(function() {
			alert( "error" );
		});
	}
</script>
@stop
