@if(Session::has('success') || Session::has('error'))
<div class="row">
	<div class="col-xs-12">
		
		<div class="alert @if(Session::has('success')) alert-success @endif @if(Session::has('error')) alert-danger @endif alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			{{ Session::get('success')	 }}
			{{ Session::get('error')	 }}
		</div>
	</div>
</div>
@endif