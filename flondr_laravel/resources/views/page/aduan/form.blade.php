		<div class="form-group">
			{!! Form::label('email','Tujuan : ') !!}
			{!! Form::text('email',null, ['class' => 'form-control', 'required' => 'required', 'disabled' => 'disabled']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('isi','Isi Pertanyaan : ') !!}
			{!! Form::textarea('isi',null, ['class' => 'form-control', 'required' => 'required', 'rows' => 5, 'disabled' => 'disabled']) !!}
		</div>
		
		<hr>
		<div class="form-group">
			{!! Form::label('subject','Subject : ') !!}
			{!! Form::text('subject',null, ['class' => 'form-control', 'required' => 'required']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('balasan','Isi Balasan : ') !!}
			{!! Form::textarea('balasan',null, ['class' => 'form-control', 'required' => 'required', 'rows' => 3]) !!}
		</div>
		<div class="form-group">
			{!! Form::submit($submitText,['class' => 'btn btn-info']) !!}
		</div>