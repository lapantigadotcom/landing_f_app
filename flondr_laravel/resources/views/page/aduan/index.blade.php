@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Contact Us</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Contact Us</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Inbox
					</h3>                                    
				</div>
				<div class="box-body table-responsive">
					@include('page.partials.notification')
					<table class="table" id="dataTables">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Identitas</th>
								<th>Isi</th>
								<th>Tlp</th>
								<!-- <th>Balasan</th> -->
								<th>Tanggal</th>
							</tr>
						</thead>
						<tbody>
							@foreach($data['content'] as $row)
							<tr>
								<td>&nbsp;</td>
								<td>
									{{ $row->nama }}
									<br>
									({{$row->email}})
								</td>
								<td>
									{{$row->isi}}
									<div class="action-post-hover">
									</div>
									@if($row->status == 0)
									<a href="{{route('vrs-admin.aduan.replyForm',[ $row->id ])}}">Balas</a>
									@endif
									<a href="javascript:void(0);" onclick="deleteModal(this)" data-href="{!! route('vrs-admin.aduan.delete',[ $row->id ]) !!}" style="color:red;">trash</a>
								</td>
								<td><strong>{{$row->phone}}</strong></td>
								<td>
									{{ date('d/m/Y',strtotime($row->created_at)) }}
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
@include('page.scripts.delete-modal')
@endsection

@section('custom-head')
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/datatables/media/css/jquery.dataTables.css') }}">
@stop
@section('custom-footer')
<script type="text/javascript" src="{{ asset('plugins/datatables/media/js/jquery.dataTables.js') }}"></script>
<script type="text/javascript">
	$('#dataTables').DataTable();
	$('#dataTables').on('click', '.Switch', function()  {
		var id_aduan = $(this).data("id");
		setFeatured(id_aduan);
		$(this).toggleClass('On').toggleClass('Off');
	});
	function setFeatured(id)
	{
		var url = "{{ URL::to('vrs-admin/aduan/featured') }}/" + id +"";
		$.ajax(url)
		.done(function() {
			
		})
		.fail(function() {
			alert( "error" );
		});
	}
</script>
@stop
