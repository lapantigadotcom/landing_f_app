@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						Edit Kategori
					</h3>                                    
				</div>
				<div class="box-body">
					{!! Form::model($data['content'],['route'=>['category.update',$data['content']->id], 'method' => 'PATCH']) !!}
					<?php 
					$arrCategory = array();
					$arrCategory[0] = 'Tidak Ada';
					foreach($data['category'] as $row)
					{
						$arrCategory[$row->id] = $row->title;
					}
					?>
					@include('page.category.form',['buttonSubmit' => 'Perbarui','arrCategory' => $arrCategory])
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection