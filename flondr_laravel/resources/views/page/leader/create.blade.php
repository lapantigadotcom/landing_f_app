@extends('app')

@section('content')
<section class="content-header">
	<h1>
		Dashboard
		<small>Control panel</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">
						TAMBAH TESTIMONI BARU
					</h3>                                    
				</div>
				<div class="box-body">
					{!! Form::open(array('url' => 'vrs-admin/testi','method' => 'post')) !!}
					<div class="col-md-9">
						@include('page.errors.list')
						@include('page.leader.form',['submitText' => 'Tambahkan'])
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
 
@endsection

@section('custom-head')

@stop
@section('custom-footer')
  
@stop
