		<div class="form-group">
			<div class="col-md-12">
				{!! Form::label('name','Nama* : ') !!}
				{!! Form::text('name',null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12">
				{!! Form::label('isi','Isi *: ') !!}
				{!! Form::textarea('isi',null, ['class' => 'form-control']) !!}
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-6">
				{!! Form::label('jabatan','Jabatan / Instansi *: ') !!}
				{!! Form::text('jabatan',null, ['class' => 'form-control']) !!}
			</div>

			<!-- <div class="col-md-6">
				{!! Form::label('facebook','Facebook : ') !!}
				{!! Form::text('facebook',null, ['class' => 'form-control']) !!}
			</div> -->
		</div>
		<!-- <div class="form-group">
			<div class="col-md-6">
				{!! Form::label('twitter','Twitter : ') !!}
				{!! Form::text('twitter',null, ['class' => 'form-control']) !!}
			</div>
			<div class="col-md-6">
				{!! Form::label('skype','Skype : ') !!}
				{!! Form::text('skype',null, ['class' => 'form-control']) !!}
			</div>
		</div> -->
		<!-- <div class="form-group" id="mediaformcontainer">
			<div class="col-md-12">
				{!! Form::label('ms_media_id','Gambar : ') !!}
				<div class="col-md-12" id="featuredImage">
				</div>
				<a href="javascript:void(0)" class="btn btn-default" onclick="setFeaturedImage()">Pilih Media</a>
				{!! Form::hidden('ms_media_id',null, ['class' => 'form-control','id' => 'ms_media_id']) !!}
			</div>
		</div> -->
		<div class="form-group"> 
			<div class="col-md-12" style="padding-top:10px">
				{!! Form::submit($submitText,['class' => 'btn btn-info']) !!}
			</div>
		</div>