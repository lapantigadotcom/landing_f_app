<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'HomeController@index');
Route::post('/subscribe', 'HomeController@subscribe');
Route::get('/blogs/{id}{page?}',array('as' => 'home.category', 'uses' => 'HomeController@category'));
Route::get('/news/{id}',array('as' => 'home.post', 'uses' => 'HomeController@post'));

Route::group(['middleware' => ['auth']], function(){
    Route::get('vrs-admin/brosur', 'GeneralController@subscription');
	Route::post('vrs-admin/brosur', 'GeneralController@subscribe');
	Route::resource('vrs-admin/aduan', 'AduanController');
	Route::get('vrs-admin/replyForm/{id}', ['as' => 'vrs-admin.aduan.replyForm', 'uses' => 'AduanController@replyForm']);
	Route::get('vrs-admin/aduan/{id}/delete', ['as' => 'vrs-admin.aduan.delete', 'uses'=>'AduanController@destroys']);
	Route::post('vrs-admin/reply/{id}', ['as' => 'vrs-admin.aduan.reply', 'uses' => 'AduanController@reply']);

	Route::get('vrs-admin/product/getAllAjax',array('as' => 'product.api.getall','uses' => 'ProductController@getAllAjax'));
	Route::get('vrs-admin/product/{product}/delete',array('as'=> 'product.delete','uses' => 'ProductController@destroy'));
	Route::resource('vrs-admin/product', 'ProductController');

	Route::get('vrs-admin/event/getAllAjax',array('as' => 'event.api.getall','uses' => 'EventController@getAllAjax'));
	Route::get('vrs-admin/event/{event}/delete',array('as'=> 'event.delete','uses' => 'EventController@destroy'));
	Route::resource('vrs-admin/event', 'EventController');

	Route::get('vrs-admin/gerai/getAllAjax',array('as' => 'gerai.api.getall','uses' => 'GeraiController@getAllAjax'));
	Route::get('vrs-admin/gerai/{gerai}/delete',array('as'=> 'gerai.delete','uses' => 'GeraiController@destroy'));
	Route::get('vrs-admin/gerai/featured/{id}', array('as' => 'gerai.featured','uses' => 'GeraiController@featured'));
	Route::resource('vrs-admin/gerai', 'GeraiController');

	Route::get('vrs-admin/testi/getAllAjax',array('as' => 'testi.api.getall','uses' => 'LeadersController@getAllAjax'));
	Route::get('vrs-admin/testi/{leader}/delete',array('as'=> 'testi.delete','uses' => 'LeadersController@destroy'));
	Route::resource('vrs-admin/testi', 'LeadersController');
	Route::get('vrs-admin/product-category/storeAjax','ProductCategoriesController@storeAjax');
	Route::get('vrs-admin/product-category/{product}/delete',array('as'=> 'product-category.delete','uses' => 'ProductCategoriesController@destroy'));
	Route::get('vrs-admin/product-category/getAllAjax',array('as' => 'product-category.api.getall','uses' => 'ProductCategoriesController@getAllAjax'));
	Route::resource('vrs-admin/product-category', 'ProductCategoriesController');

	Route::get('vrs-admin/achievement/storeAjax','AchievementController@storeAjax');
	Route::get('vrs-admin/achievement/{leader}/delete',array('as'=> 'achievement.delete','uses' => 'AchievementController@destroy'));
	Route::get('vrs-admin/achievement/getAllAjax',array('as' => 'achievement.api.getall','uses' => 'AchievementController@getAllAjax'));
	Route::resource('vrs-admin/achievement', 'AchievementController');
	
	Route::get('vrs-admin/slider/{slider}/delete',array('as'=> 'slider.delete','uses' => 'SliderController@destroy'));
	Route::resource('vrs-admin/slider', 'SliderController');

	Route::resource('vrs-admin/cctv', 'CCTVController');
	Route::get('vrs-admin/cctv/{cctv}/delete',array('as'=> 'cctv.delete','uses' => 'CCTVController@destroy'));

	Route::resource('vrs-admin/theme', 'ThemeController');
	Route::get('vrs-admin/club/{club}/delete',array('as'=> 'club.delete','uses' => 'ClubController@destroy'));
	Route::resource('vrs-admin/club', 'ClubController');

	Route::get('vrs-admin/portfolio/enabled/{id}/{menu}',array('as'=> 'portfolio.enabled','uses' => 'PortfolioController@enabled'));
	Route::get('vrs-admin/portfolio/featured/{id}', array('as' => 'portfolio.featured','uses' => 'PortfolioController@featured'));
	Route::get('vrs-admin/portfolio/{menu}/delete',array('as'=> 'portfolio.delete','uses' => 'PortfolioController@destroy'));

	Route::get('vrs-admin/detailportfolio/{menu}/delete',array('as'=> 'detailportfolio.delete','uses' => 'DetailPortfolioController@destroy'));
	Route::resource('vrs-admin/detailportfolio', 'DetailPortfolioController');

	Route::get('vrs-admin/portfolio/getAllData',array('as' => 'portfolio.api.getall','uses' => 'PortfolioController@getAllData'));
	Route::resource('vrs-admin/portfolio', 'PortfolioController');

	Route::get('vrs-admin/menu/{menu}/delete',array('as'=> 'menu.delete','uses' => 'MenuController@destroy'));
	Route::resource('vrs-admin/menu', 'MenuController');
	Route::get('vrs-admin/menu/detail/{menu}/delete',array('as'=> 'menu.detail.delete','uses' => 'MenuDetailController@destroy'));
	Route::get('vrs-admin/menu/detail/getjson',array('as'=> 'menu.detail.getjson','uses' => 'MenuDetailController@getjson'));
	Route::resource('vrs-admin/menu/detail', 'MenuDetailController');

	Route::resource('vrs-admin/ujian', 'UjianController');
	Route::get('vrs-admin/ujian/{ujian}/delete',array('as'=> 'ujian.delete','uses' => 'UjianController@destroy'));

	Route::resource('vrs-admin/soal', 'SoalController');
	Route::get('vrs-admin/soal/{soal}/delete',array('as'=> 'soal.delete','uses' => 'SoalController@destroy'));

	Route::post('vrs-admin/general/upload',array('as' => 'general.upload', 'uses' => 'GeneralController@upload'));
	Route::resource('vrs-admin/general', 'GeneralController');

	Route::get('vrs-admin/contact/enabled/{id}',array('as' => 'contact.enabled', 'uses' => 'ContactController@enabled'));
	Route::get('vrs-admin/contact/{contact}/delete',array('as'=> 'contact.delete','uses' => 'ContactController@destroy'));
	Route::resource('vrs-admin/contact', 'ContactController');
	
	Route::resource('vrs-admin/dailycp', 'DailyCPController');

	Route::get('vrs-admin/rss/getAllAjax',array('as' => 'rss.api.getall','uses' => 'RssController@getAllAjax'));
	Route::get('vrs-admin/rss/{rss}/delete',array('as'=> 'rss.delete','uses' => 'RssController@destroy'));
	Route::resource('vrs-admin/rss', 'RssController');

	Route::get('vrs-admin/social/{social}/delete',array('as'=> 'social.delete','uses' => 'SocialController@destroy'));
	Route::resource('vrs-admin/social', 'SocialController');

	Route::get('vrs-admin/comment/{comment}/delete',array('as'=> 'comment.delete','uses' => 'CommentController@destroy'));
	Route::get('vrs-admin/comment/{comment}/approve/{on}',array('as'=> 'comment.approve','uses' => 'CommentController@approve'));
	Route::resource('vrs-admin/comment','CommentController');

	Route::get('vrs-admin/typeclub/{id}/delete',array('as'=> 'typeclub.delete','uses' => 'TypeClubController@destroy'));
	Route::resource('vrs-admin/typeclub','TypeClubController');

	Route::get('vrs-admin/portfoliocategory/getAllAjax',array('as' => 'portfoliocategory.api.getall','uses' => 'PortfolioCategoryController@getAllAjax'));

	//Route::get('vrs-admin/notification/getAllAjax',array('as' => 'post.api.getall','uses' => 'PostController@getAllAjax'));
	Route::get('vrs-admin/notification/create',array('as'=> 'notification.create','uses' => 'NotificationController@create'));
	Route::resource('vrs-admin/notification', 'NotificationController');
	Route::get('vrs-admin', 'AdminController@index');
	Route::get('vrs-admin/createnotification', 'AdminController@createNotification');
	Route::post('vrs-admin/password', array('as' => 'admin.password', 'uses' => 'AdminController@Changepassword'));
	Route::get('vrs-admin/password', array('as' => 'admin.password', 'uses' => 'AdminController@password'));

	Route::get('vrs-admin/post/getAllAjax',array('as' => 'post.api.getall','uses' => 'PostController@getAllAjax'));
	Route::get('vrs-admin/post/{post}/delete',array('as'=> 'post.delete','uses' => 'PostController@destroy'));
	Route::get('vrs-admin/post/featured/{id}', array('as' => 'post.featured','uses' => 'PostController@featured'));
	Route::resource('vrs-admin/post', 'PostController');

	Route::get('vrs-admin/category/storeAjax','CategoryController@storeAjax');
	Route::get('vrs-admin/category/{post}/delete',array('as'=> 'category.delete','uses' => 'CategoryController@destroy'));
	Route::get('vrs-admin/category/getAllAjax',array('as' => 'category.api.getall','uses' => 'CategoryController@getAllAjax'));
	Route::resource('vrs-admin/category', 'CategoryController');

	Route::get('vrs-admin/tag/storeAjax','TagController@storeAjax');
	Route::get('vrs-admin/tag/{tag}/delete',array('as'=> 'tag.delete','uses' => 'TagController@destroy'));
	Route::resource('vrs-admin/tag', 'TagController');

	Route::post('vrs-admin/media/storeAjax','MediaController@storeAjax');
	Route::get('vrs-admin/media/getAllData','MediaController@getAllData');
	Route::get('vrs-admin/media/getMedia/{name}','MediaController@getMedia');
	Route::get('vrs-admin/media/{media}/delete',array('as'=> 'media.delete','uses' => 'MediaController@destroy'));
	
	Route::resource('vrs-admin/media', 'MediaController');
	
	Route::get('vrs-admin/file/getMedia/{name}','FileController@getMedia');
	//Route::post('vrs-admin/file/create',array('as'=> 'file.create','uses' => 'FileController@create'));
	Route::get('vrs-admin/file/{file}/delete',array('as'=> 'file.delete','uses' => 'FileController@destroy'));
	Route::get('vrs-admin/file/featured/{id}', array('as' => 'file.featured','uses' => 'FileController@featured'));
	Route::resource('vrs-admin/file', 'FileController');

	Route::get('vrs-admin/video/getMedia/{name}','VideoController@getMedia');
	Route::get('vrs-admin/video/{video}/delete',array('as'=> 'video.delete','uses' => 'VideoController@destroy'));
	Route::get('vrs-admin/video/featured/{id}', array('as' => 'video.featured','uses' => 'VideoController@featured'));
	Route::resource('vrs-admin/video', 'VideoController');

	Route::any('/imglist', ['as'=>'imglist', 'uses'=>'FileBrowserController@imageList']);
		//Image upload

		Route::any('/upload', [ 'uses' =>'FileBrowserController@index']);
});

Route::get('read/{id}', 'PostController@show');
Route::get('/map', 'HomeController@map');
Route::post('/adukan', ['as' => 'home.adukan', 'uses' => 'HomeController@adukan']);

Route::get('post/search', 'PostController@search');
// Auth::routes();
Route::group(['middleware' => ['web']], function() {

// Login Routes...
    Route::get('flondr/backend/', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
    Route::post('flondr/backend/', ['as' => 'login.post', 'uses' => 'Auth\LoginController@login']);
    Route::post('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

// Registration Routes...
    Route::get('register', ['as' => 'register', 'uses' => 'Auth\RegisterController@showRegistrationForm']);
    Route::post('register', ['as' => 'register.post', 'uses' => 'Auth\RegisterController@register']);

// Password Reset Routes...
    Route::get('password/reset', ['as' => 'password.reset', 'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
    Route::post('password/email', ['as' => 'password.email', 'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail']);
    Route::get('password/reset/{token}', ['as' => 'password.reset.token', 'uses' => 'Auth\ResetPasswordController@showResetForm']);
    Route::post('password/reset', ['as' => 'password.reset.post', 'uses' => 'Auth\ResetPasswordController@reset']);
});

Route::get('auth/logout', function(){
	Auth::logout();
	return redirect('/');
});
Route::get('/vrs-admin', 'AdminController@index');
